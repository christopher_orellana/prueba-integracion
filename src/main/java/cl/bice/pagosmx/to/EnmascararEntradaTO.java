package cl.bice.pagosmx.to;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * EnmascararEntradaTO
 */
@Validated




public class EnmascararEntradaTO   {
  @JsonProperty("numeroProducto")
  private String numeroProducto = null;

  public EnmascararEntradaTO numeroProducto(String numeroProducto) {
    this.numeroProducto = numeroProducto;
    return this;
  }

  /**
   * numero producto
   * @return numeroProducto
  **/
  @ApiModelProperty(value = "numero producto")


  public String getNumeroProducto() {
    return numeroProducto;
  }

  public void setNumeroProducto(String numeroProducto) {
    this.numeroProducto = numeroProducto;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EnmascararEntradaTO enmascararEntradaTO = (EnmascararEntradaTO) o;
    return Objects.equals(this.numeroProducto, enmascararEntradaTO.numeroProducto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(numeroProducto);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EnmascararEntradaTO {\n");
    
    sb.append("    numeroProducto: ").append(toIndentedString(numeroProducto)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

