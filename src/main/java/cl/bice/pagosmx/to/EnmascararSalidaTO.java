package cl.bice.pagosmx.to;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * EnmascararSalidaTO
 */
@Validated




public class EnmascararSalidaTO   {
  @JsonProperty("codigoSalida")
  private String codigoSalida = null;

  @JsonProperty("glosaSalida")
  private String glosaSalida = null;

  @JsonProperty("mascara")
  private String mascara = null;

  public EnmascararSalidaTO codigoSalida(String codigoSalida) {
    this.codigoSalida = codigoSalida;
    return this;
  }

  /**
   * codigo de salida
   * @return codigoSalida
  **/
  @ApiModelProperty(value = "codigo de salida")


  public String getCodigoSalida() {
    return codigoSalida;
  }

  public void setCodigoSalida(String codigoSalida) {
    this.codigoSalida = codigoSalida;
  }

  public EnmascararSalidaTO glosaSalida(String glosaSalida) {
    this.glosaSalida = glosaSalida;
    return this;
  }

  /**
   * glosa de salida
   * @return glosaSalida
  **/
  @ApiModelProperty(value = "glosa de salida")


  public String getGlosaSalida() {
    return glosaSalida;
  }

  public void setGlosaSalida(String glosaSalida) {
    this.glosaSalida = glosaSalida;
  }

  public EnmascararSalidaTO mascara(String mascara) {
    this.mascara = mascara;
    return this;
  }

  /**
   * mascara
   * @return mascara
  **/
  @ApiModelProperty(value = "mascara")


  public String getMascara() {
    return mascara;
  }

  public void setMascara(String mascara) {
    this.mascara = mascara;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EnmascararSalidaTO enmascararSalidaTO = (EnmascararSalidaTO) o;
    return Objects.equals(this.codigoSalida, enmascararSalidaTO.codigoSalida) &&
        Objects.equals(this.glosaSalida, enmascararSalidaTO.glosaSalida) &&
        Objects.equals(this.mascara, enmascararSalidaTO.mascara);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoSalida, glosaSalida, mascara);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EnmascararSalidaTO {\n");
    
    sb.append("    codigoSalida: ").append(toIndentedString(codigoSalida)).append("\n");
    sb.append("    glosaSalida: ").append(toIndentedString(glosaSalida)).append("\n");
    sb.append("    mascara: ").append(toIndentedString(mascara)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

