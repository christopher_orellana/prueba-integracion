package cl.bice.pagosmx.to;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DesenmascararSalidaTO
 */
@Validated




public class DesenmascararSalidaTO   {
  @JsonProperty("codigoSalida")
  private String codigoSalida = null;

  @JsonProperty("glosaSalida")
  private String glosaSalida = null;

  @JsonProperty("numeroProducto")
  private String numeroProducto = null;

  public DesenmascararSalidaTO codigoSalida(String codigoSalida) {
    this.codigoSalida = codigoSalida;
    return this;
  }

  /**
   * codigo de salida
   * @return codigoSalida
  **/
  @ApiModelProperty(value = "codigo de salida")


  public String getCodigoSalida() {
    return codigoSalida;
  }

  public void setCodigoSalida(String codigoSalida) {
    this.codigoSalida = codigoSalida;
  }

  public DesenmascararSalidaTO glosaSalida(String glosaSalida) {
    this.glosaSalida = glosaSalida;
    return this;
  }

  /**
   * glosa de salida
   * @return glosaSalida
  **/
  @ApiModelProperty(value = "glosa de salida")


  public String getGlosaSalida() {
    return glosaSalida;
  }

  public void setGlosaSalida(String glosaSalida) {
    this.glosaSalida = glosaSalida;
  }

  public DesenmascararSalidaTO numeroProducto(String numeroProducto) {
    this.numeroProducto = numeroProducto;
    return this;
  }

  /**
   * numero producto
   * @return numeroProducto
  **/
  @ApiModelProperty(value = "numero producto")


  public String getNumeroProducto() {
    return numeroProducto;
  }

  public void setNumeroProducto(String numeroProducto) {
    this.numeroProducto = numeroProducto;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DesenmascararSalidaTO desenmascararSalidaTO = (DesenmascararSalidaTO) o;
    return Objects.equals(this.codigoSalida, desenmascararSalidaTO.codigoSalida) &&
        Objects.equals(this.glosaSalida, desenmascararSalidaTO.glosaSalida) &&
        Objects.equals(this.numeroProducto, desenmascararSalidaTO.numeroProducto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoSalida, glosaSalida, numeroProducto);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DesenmascararSalidaTO {\n");
    
    sb.append("    codigoSalida: ").append(toIndentedString(codigoSalida)).append("\n");
    sb.append("    glosaSalida: ").append(toIndentedString(glosaSalida)).append("\n");
    sb.append("    numeroProducto: ").append(toIndentedString(numeroProducto)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

