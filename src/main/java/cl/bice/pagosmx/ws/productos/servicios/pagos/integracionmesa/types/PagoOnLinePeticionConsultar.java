
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PagoOnLinePeticionConsultar complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PagoOnLinePeticionConsultar"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Consultas" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}mesaDeDineroPagoOnLinePeticionConsultarType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagoOnLinePeticionConsultar", propOrder = {
    "consultas"
})
public class PagoOnLinePeticionConsultar {

    @XmlElement(name = "Consultas", required = true, nillable = true)
    protected MesaDeDineroPagoOnLinePeticionConsultarType consultas;

    /**
     * Obtiene el valor de la propiedad consultas.
     * 
     * @return
     *     possible object is
     *     {@link MesaDeDineroPagoOnLinePeticionConsultarType }
     *     
     */
    public MesaDeDineroPagoOnLinePeticionConsultarType getConsultas() {
        return consultas;
    }

    /**
     * Define el valor de la propiedad consultas.
     * 
     * @param value
     *     allowed object is
     *     {@link MesaDeDineroPagoOnLinePeticionConsultarType }
     *     
     */
    public void setConsultas(MesaDeDineroPagoOnLinePeticionConsultarType value) {
        this.consultas = value;
    }

}
