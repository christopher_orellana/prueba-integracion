
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PagoOnLinePeticionIngreso complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PagoOnLinePeticionIngreso"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="pagos" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}mesaDeDineroPagoOnLinePeticionIngresoType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagoOnLinePeticionIngreso", propOrder = {
    "pagos"
})
public class PagoOnLinePeticionIngreso {

    @XmlElement(required = true, nillable = true)
    protected MesaDeDineroPagoOnLinePeticionIngresoType pagos;

    /**
     * Obtiene el valor de la propiedad pagos.
     * 
     * @return
     *     possible object is
     *     {@link MesaDeDineroPagoOnLinePeticionIngresoType }
     *     
     */
    public MesaDeDineroPagoOnLinePeticionIngresoType getPagos() {
        return pagos;
    }

    /**
     * Define el valor de la propiedad pagos.
     * 
     * @param value
     *     allowed object is
     *     {@link MesaDeDineroPagoOnLinePeticionIngresoType }
     *     
     */
    public void setPagos(MesaDeDineroPagoOnLinePeticionIngresoType value) {
        this.pagos = value;
    }

}
