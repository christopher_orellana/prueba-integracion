package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.IntegracionMesaBinding;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.4.3
 * 2021-03-23T10:38:26.008-03:00
 * Generated source version: 3.4.3
 *
 */
@WebServiceClient(name = "IntegracionMDP",
                  wsdlLocation = "http://10.150.238.91/Setup_wsIntegracionMDP/WSIntegracionMDP.asmx?wsdl",
                  targetNamespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa/")
public class IntegracionMDP extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa/", "IntegracionMDP");
    public final static QName IntegracionMesaBinding1 = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa/", "IntegracionMesaBinding1");
    public final static QName IntegracionMesaBinding = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa/", "IntegracionMesaBinding");
    static {
        URL url = null;
        try {
            url = new URL("http://10.150.238.91/Setup_wsIntegracionMDP/WSIntegracionMDP.asmx?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(IntegracionMDP.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://10.150.238.91/Setup_wsIntegracionMDP/WSIntegracionMDP.asmx?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public IntegracionMDP(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public IntegracionMDP(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public IntegracionMDP() {
        super(WSDL_LOCATION, SERVICE);
    }

    public IntegracionMDP(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public IntegracionMDP(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public IntegracionMDP(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns IntegracionMesaBinding
     */
    @WebEndpoint(name = "IntegracionMesaBinding1")
    public IntegracionMesaBinding getIntegracionMesaBinding1() {
        return super.getPort(IntegracionMesaBinding1, IntegracionMesaBinding.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IntegracionMesaBinding
     */
    @WebEndpoint(name = "IntegracionMesaBinding1")
    public IntegracionMesaBinding getIntegracionMesaBinding1(WebServiceFeature... features) {
        return super.getPort(IntegracionMesaBinding1, IntegracionMesaBinding.class, features);
    }


    /**
     *
     * @return
     *     returns IntegracionMesaBinding
     */
    @WebEndpoint(name = "IntegracionMesaBinding")
    public IntegracionMesaBinding getIntegracionMesaBinding() {
        return super.getPort(IntegracionMesaBinding, IntegracionMesaBinding.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IntegracionMesaBinding
     */
    @WebEndpoint(name = "IntegracionMesaBinding")
    public IntegracionMesaBinding getIntegracionMesaBinding(WebServiceFeature... features) {
        return super.getPort(IntegracionMesaBinding, IntegracionMesaBinding.class, features);
    }

}
