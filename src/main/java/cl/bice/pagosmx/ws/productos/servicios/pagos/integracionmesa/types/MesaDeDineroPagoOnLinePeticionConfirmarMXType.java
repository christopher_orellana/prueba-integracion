
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para mesaDeDineroPagoOnLinePeticionConfirmarMXType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="mesaDeDineroPagoOnLinePeticionConfirmarMXType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="TipoOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NumeroOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="IndicadorPagoCobro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BancoReceptor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TipoAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AlcanceOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorBloqueo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorSolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyA_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyA_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyA_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyA_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyA_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyB_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyB_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyB_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyB_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PartyB_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FondoBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CondicionesTerminos_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CondicionesTerminos_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CondicionesTerminos_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CondicionesTerminos_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CondicionesTerminos_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CondicionesTerminos_6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HoraOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HoraValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TipoCambio" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="Moneda_SecB1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Monto_SecB1" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="AgenteEntrega_SecB1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB1_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB1_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB1_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB1_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB1_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB1_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB1_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB1_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB1_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB1_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Moneda_SecB2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Monto_SecB2" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="AgenteEntrega_SecB2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB2_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB2_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB2_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB2_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecB2_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB2_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB2_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB2_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB2_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecB2_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InstitucionBeneficiario_SecB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionContacto_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionContacto_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionContacto_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionContacto_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MetodoTratar_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MetodoTratar_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MetodoTratar_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MetodoTratar_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos1_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos1_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos1_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos1_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos1_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos2_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos2_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos2_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos2_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Tratos2_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IdBroker_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IdBroker_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IdBroker_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IdBroker_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IdBroker_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ComisionBroker_Moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ComisionBroker_Monto" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContraparte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaBroker" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorCompraVenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Moneda_SecD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Monto_SecD" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="AgenteEntrega_SecD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecD_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecD_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecD_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecD_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Intermediario_SecD_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecD_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecD_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecD_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecD_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AgenteReceptor_SecD_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InstitucionBeneficiario_SecD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NumeroEstablecimientos" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mesaDeDineroPagoOnLinePeticionConfirmarMXType", propOrder = {
    "tipoOperacion",
    "numeroOperacion",
    "indicadorPagoCobro",
    "codigoProducto",
    "bancoReceptor",
    "tipoAccion",
    "alcanceOperacion",
    "indicadorBloqueo",
    "indicadorSolucion",
    "partyA1",
    "partyA2",
    "partyA3",
    "partyA4",
    "partyA5",
    "partyB1",
    "partyB2",
    "partyB3",
    "partyB4",
    "partyB5",
    "fondoBeneficiario",
    "condicionesTerminos1",
    "condicionesTerminos2",
    "condicionesTerminos3",
    "condicionesTerminos4",
    "condicionesTerminos5",
    "condicionesTerminos6",
    "fechaOperacion",
    "horaOperacion",
    "fechaValor",
    "horaValor",
    "tipoCambio",
    "monedaSecB1",
    "montoSecB1",
    "agenteEntregaSecB1",
    "intermediarioSecB11",
    "intermediarioSecB12",
    "intermediarioSecB13",
    "intermediarioSecB14",
    "intermediarioSecB15",
    "agenteReceptorSecB11",
    "agenteReceptorSecB12",
    "agenteReceptorSecB13",
    "agenteReceptorSecB14",
    "agenteReceptorSecB15",
    "monedaSecB2",
    "montoSecB2",
    "agenteEntregaSecB2",
    "intermediarioSecB21",
    "intermediarioSecB22",
    "intermediarioSecB23",
    "intermediarioSecB24",
    "intermediarioSecB25",
    "agenteReceptorSecB21",
    "agenteReceptorSecB22",
    "agenteReceptorSecB23",
    "agenteReceptorSecB24",
    "agenteReceptorSecB25",
    "institucionBeneficiarioSecB",
    "informacionContacto1",
    "informacionContacto2",
    "informacionContacto3",
    "informacionContacto4",
    "metodoTratar1",
    "metodoTratar2",
    "metodoTratar3",
    "metodoTratar4",
    "tratos11",
    "tratos12",
    "tratos13",
    "tratos14",
    "tratos15",
    "tratos21",
    "tratos22",
    "tratos23",
    "tratos24",
    "tratos25",
    "idBroker1",
    "idBroker2",
    "idBroker3",
    "idBroker4",
    "idBroker5",
    "comisionBrokerMoneda",
    "comisionBrokerMonto",
    "referenciaContraparte",
    "referenciaBroker",
    "informacionRemitente1",
    "informacionRemitente2",
    "informacionRemitente3",
    "informacionRemitente4",
    "informacionRemitente5",
    "informacionRemitente6",
    "indicadorCompraVenta",
    "monedaSecD",
    "montoSecD",
    "agenteEntregaSecD",
    "intermediarioSecD1",
    "intermediarioSecD2",
    "intermediarioSecD3",
    "intermediarioSecD4",
    "intermediarioSecD5",
    "agenteReceptorSecD1",
    "agenteReceptorSecD2",
    "agenteReceptorSecD3",
    "agenteReceptorSecD4",
    "agenteReceptorSecD5",
    "institucionBeneficiarioSecD",
    "numeroEstablecimientos"
})
public class MesaDeDineroPagoOnLinePeticionConfirmarMXType {

    @XmlElement(name = "TipoOperacion")
    protected String tipoOperacion;
    @XmlElement(name = "NumeroOperacion")
    protected int numeroOperacion;
    @XmlElement(name = "IndicadorPagoCobro")
    protected String indicadorPagoCobro;
    @XmlElement(name = "CodigoProducto")
    protected String codigoProducto;
    @XmlElement(name = "BancoReceptor")
    protected String bancoReceptor;
    @XmlElement(name = "TipoAccion")
    protected String tipoAccion;
    @XmlElement(name = "AlcanceOperacion")
    protected String alcanceOperacion;
    @XmlElement(name = "IndicadorBloqueo")
    protected String indicadorBloqueo;
    @XmlElement(name = "IndicadorSolucion")
    protected String indicadorSolucion;
    @XmlElement(name = "PartyA_1")
    protected String partyA1;
    @XmlElement(name = "PartyA_2")
    protected String partyA2;
    @XmlElement(name = "PartyA_3")
    protected String partyA3;
    @XmlElement(name = "PartyA_4")
    protected String partyA4;
    @XmlElement(name = "PartyA_5")
    protected String partyA5;
    @XmlElement(name = "PartyB_1")
    protected String partyB1;
    @XmlElement(name = "PartyB_2")
    protected String partyB2;
    @XmlElement(name = "PartyB_3")
    protected String partyB3;
    @XmlElement(name = "PartyB_4")
    protected String partyB4;
    @XmlElement(name = "PartyB_5")
    protected String partyB5;
    @XmlElement(name = "FondoBeneficiario")
    protected String fondoBeneficiario;
    @XmlElement(name = "CondicionesTerminos_1")
    protected String condicionesTerminos1;
    @XmlElement(name = "CondicionesTerminos_2")
    protected String condicionesTerminos2;
    @XmlElement(name = "CondicionesTerminos_3")
    protected String condicionesTerminos3;
    @XmlElement(name = "CondicionesTerminos_4")
    protected String condicionesTerminos4;
    @XmlElement(name = "CondicionesTerminos_5")
    protected String condicionesTerminos5;
    @XmlElement(name = "CondicionesTerminos_6")
    protected String condicionesTerminos6;
    @XmlElement(name = "FechaOperacion")
    protected String fechaOperacion;
    @XmlElement(name = "HoraOperacion")
    protected String horaOperacion;
    @XmlElement(name = "FechaValor")
    protected String fechaValor;
    @XmlElement(name = "HoraValor")
    protected String horaValor;
    @XmlElement(name = "TipoCambio", required = true)
    protected BigDecimal tipoCambio;
    @XmlElement(name = "Moneda_SecB1")
    protected String monedaSecB1;
    @XmlElement(name = "Monto_SecB1", required = true)
    protected BigDecimal montoSecB1;
    @XmlElement(name = "AgenteEntrega_SecB1")
    protected String agenteEntregaSecB1;
    @XmlElement(name = "Intermediario_SecB1_1")
    protected String intermediarioSecB11;
    @XmlElement(name = "Intermediario_SecB1_2")
    protected String intermediarioSecB12;
    @XmlElement(name = "Intermediario_SecB1_3")
    protected String intermediarioSecB13;
    @XmlElement(name = "Intermediario_SecB1_4")
    protected String intermediarioSecB14;
    @XmlElement(name = "Intermediario_SecB1_5")
    protected String intermediarioSecB15;
    @XmlElement(name = "AgenteReceptor_SecB1_1")
    protected String agenteReceptorSecB11;
    @XmlElement(name = "AgenteReceptor_SecB1_2")
    protected String agenteReceptorSecB12;
    @XmlElement(name = "AgenteReceptor_SecB1_3")
    protected String agenteReceptorSecB13;
    @XmlElement(name = "AgenteReceptor_SecB1_4")
    protected String agenteReceptorSecB14;
    @XmlElement(name = "AgenteReceptor_SecB1_5")
    protected String agenteReceptorSecB15;
    @XmlElement(name = "Moneda_SecB2")
    protected String monedaSecB2;
    @XmlElement(name = "Monto_SecB2", required = true)
    protected BigDecimal montoSecB2;
    @XmlElement(name = "AgenteEntrega_SecB2")
    protected String agenteEntregaSecB2;
    @XmlElement(name = "Intermediario_SecB2_1")
    protected String intermediarioSecB21;
    @XmlElement(name = "Intermediario_SecB2_2")
    protected String intermediarioSecB22;
    @XmlElement(name = "Intermediario_SecB2_3")
    protected String intermediarioSecB23;
    @XmlElement(name = "Intermediario_SecB2_4")
    protected String intermediarioSecB24;
    @XmlElement(name = "Intermediario_SecB2_5")
    protected String intermediarioSecB25;
    @XmlElement(name = "AgenteReceptor_SecB2_1")
    protected String agenteReceptorSecB21;
    @XmlElement(name = "AgenteReceptor_SecB2_2")
    protected String agenteReceptorSecB22;
    @XmlElement(name = "AgenteReceptor_SecB2_3")
    protected String agenteReceptorSecB23;
    @XmlElement(name = "AgenteReceptor_SecB2_4")
    protected String agenteReceptorSecB24;
    @XmlElement(name = "AgenteReceptor_SecB2_5")
    protected String agenteReceptorSecB25;
    @XmlElement(name = "InstitucionBeneficiario_SecB")
    protected String institucionBeneficiarioSecB;
    @XmlElement(name = "InformacionContacto_1")
    protected String informacionContacto1;
    @XmlElement(name = "InformacionContacto_2")
    protected String informacionContacto2;
    @XmlElement(name = "InformacionContacto_3")
    protected String informacionContacto3;
    @XmlElement(name = "InformacionContacto_4")
    protected String informacionContacto4;
    @XmlElement(name = "MetodoTratar_1")
    protected String metodoTratar1;
    @XmlElement(name = "MetodoTratar_2")
    protected String metodoTratar2;
    @XmlElement(name = "MetodoTratar_3")
    protected String metodoTratar3;
    @XmlElement(name = "MetodoTratar_4")
    protected String metodoTratar4;
    @XmlElement(name = "Tratos1_1")
    protected String tratos11;
    @XmlElement(name = "Tratos1_2")
    protected String tratos12;
    @XmlElement(name = "Tratos1_3")
    protected String tratos13;
    @XmlElement(name = "Tratos1_4")
    protected String tratos14;
    @XmlElement(name = "Tratos1_5")
    protected String tratos15;
    @XmlElement(name = "Tratos2_1")
    protected String tratos21;
    @XmlElement(name = "Tratos2_2")
    protected String tratos22;
    @XmlElement(name = "Tratos2_3")
    protected String tratos23;
    @XmlElement(name = "Tratos2_4")
    protected String tratos24;
    @XmlElement(name = "Tratos2_5")
    protected String tratos25;
    @XmlElement(name = "IdBroker_1")
    protected String idBroker1;
    @XmlElement(name = "IdBroker_2")
    protected String idBroker2;
    @XmlElement(name = "IdBroker_3")
    protected String idBroker3;
    @XmlElement(name = "IdBroker_4")
    protected String idBroker4;
    @XmlElement(name = "IdBroker_5")
    protected String idBroker5;
    @XmlElement(name = "ComisionBroker_Moneda")
    protected String comisionBrokerMoneda;
    @XmlElement(name = "ComisionBroker_Monto", required = true)
    protected BigDecimal comisionBrokerMonto;
    @XmlElement(name = "ReferenciaContraparte")
    protected String referenciaContraparte;
    @XmlElement(name = "ReferenciaBroker")
    protected String referenciaBroker;
    @XmlElement(name = "InformacionRemitente_1")
    protected String informacionRemitente1;
    @XmlElement(name = "InformacionRemitente_2")
    protected String informacionRemitente2;
    @XmlElement(name = "InformacionRemitente_3")
    protected String informacionRemitente3;
    @XmlElement(name = "InformacionRemitente_4")
    protected String informacionRemitente4;
    @XmlElement(name = "InformacionRemitente_5")
    protected String informacionRemitente5;
    @XmlElement(name = "InformacionRemitente_6")
    protected String informacionRemitente6;
    @XmlElement(name = "IndicadorCompraVenta")
    protected String indicadorCompraVenta;
    @XmlElement(name = "Moneda_SecD")
    protected String monedaSecD;
    @XmlElement(name = "Monto_SecD", required = true)
    protected BigDecimal montoSecD;
    @XmlElement(name = "AgenteEntrega_SecD")
    protected String agenteEntregaSecD;
    @XmlElement(name = "Intermediario_SecD_1")
    protected String intermediarioSecD1;
    @XmlElement(name = "Intermediario_SecD_2")
    protected String intermediarioSecD2;
    @XmlElement(name = "Intermediario_SecD_3")
    protected String intermediarioSecD3;
    @XmlElement(name = "Intermediario_SecD_4")
    protected String intermediarioSecD4;
    @XmlElement(name = "Intermediario_SecD_5")
    protected String intermediarioSecD5;
    @XmlElement(name = "AgenteReceptor_SecD_1")
    protected String agenteReceptorSecD1;
    @XmlElement(name = "AgenteReceptor_SecD_2")
    protected String agenteReceptorSecD2;
    @XmlElement(name = "AgenteReceptor_SecD_3")
    protected String agenteReceptorSecD3;
    @XmlElement(name = "AgenteReceptor_SecD_4")
    protected String agenteReceptorSecD4;
    @XmlElement(name = "AgenteReceptor_SecD_5")
    protected String agenteReceptorSecD5;
    @XmlElement(name = "InstitucionBeneficiario_SecD")
    protected String institucionBeneficiarioSecD;
    @XmlElement(name = "NumeroEstablecimientos", required = true)
    protected BigDecimal numeroEstablecimientos;

    /**
     * Obtiene el valor de la propiedad tipoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * Define el valor de la propiedad tipoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperacion(String value) {
        this.tipoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroOperacion.
     * 
     */
    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    /**
     * Define el valor de la propiedad numeroOperacion.
     * 
     */
    public void setNumeroOperacion(int value) {
        this.numeroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorPagoCobro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorPagoCobro() {
        return indicadorPagoCobro;
    }

    /**
     * Define el valor de la propiedad indicadorPagoCobro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorPagoCobro(String value) {
        this.indicadorPagoCobro = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Define el valor de la propiedad codigoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProducto(String value) {
        this.codigoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad bancoReceptor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBancoReceptor() {
        return bancoReceptor;
    }

    /**
     * Define el valor de la propiedad bancoReceptor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBancoReceptor(String value) {
        this.bancoReceptor = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAccion() {
        return tipoAccion;
    }

    /**
     * Define el valor de la propiedad tipoAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAccion(String value) {
        this.tipoAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad alcanceOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlcanceOperacion() {
        return alcanceOperacion;
    }

    /**
     * Define el valor de la propiedad alcanceOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlcanceOperacion(String value) {
        this.alcanceOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorBloqueo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorBloqueo() {
        return indicadorBloqueo;
    }

    /**
     * Define el valor de la propiedad indicadorBloqueo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorBloqueo(String value) {
        this.indicadorBloqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorSolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorSolucion() {
        return indicadorSolucion;
    }

    /**
     * Define el valor de la propiedad indicadorSolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorSolucion(String value) {
        this.indicadorSolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad partyA1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyA1() {
        return partyA1;
    }

    /**
     * Define el valor de la propiedad partyA1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyA1(String value) {
        this.partyA1 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyA2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyA2() {
        return partyA2;
    }

    /**
     * Define el valor de la propiedad partyA2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyA2(String value) {
        this.partyA2 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyA3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyA3() {
        return partyA3;
    }

    /**
     * Define el valor de la propiedad partyA3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyA3(String value) {
        this.partyA3 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyA4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyA4() {
        return partyA4;
    }

    /**
     * Define el valor de la propiedad partyA4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyA4(String value) {
        this.partyA4 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyA5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyA5() {
        return partyA5;
    }

    /**
     * Define el valor de la propiedad partyA5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyA5(String value) {
        this.partyA5 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyB1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyB1() {
        return partyB1;
    }

    /**
     * Define el valor de la propiedad partyB1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyB1(String value) {
        this.partyB1 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyB2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyB2() {
        return partyB2;
    }

    /**
     * Define el valor de la propiedad partyB2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyB2(String value) {
        this.partyB2 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyB3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyB3() {
        return partyB3;
    }

    /**
     * Define el valor de la propiedad partyB3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyB3(String value) {
        this.partyB3 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyB4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyB4() {
        return partyB4;
    }

    /**
     * Define el valor de la propiedad partyB4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyB4(String value) {
        this.partyB4 = value;
    }

    /**
     * Obtiene el valor de la propiedad partyB5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyB5() {
        return partyB5;
    }

    /**
     * Define el valor de la propiedad partyB5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyB5(String value) {
        this.partyB5 = value;
    }

    /**
     * Obtiene el valor de la propiedad fondoBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFondoBeneficiario() {
        return fondoBeneficiario;
    }

    /**
     * Define el valor de la propiedad fondoBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFondoBeneficiario(String value) {
        this.fondoBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad condicionesTerminos1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondicionesTerminos1() {
        return condicionesTerminos1;
    }

    /**
     * Define el valor de la propiedad condicionesTerminos1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondicionesTerminos1(String value) {
        this.condicionesTerminos1 = value;
    }

    /**
     * Obtiene el valor de la propiedad condicionesTerminos2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondicionesTerminos2() {
        return condicionesTerminos2;
    }

    /**
     * Define el valor de la propiedad condicionesTerminos2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondicionesTerminos2(String value) {
        this.condicionesTerminos2 = value;
    }

    /**
     * Obtiene el valor de la propiedad condicionesTerminos3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondicionesTerminos3() {
        return condicionesTerminos3;
    }

    /**
     * Define el valor de la propiedad condicionesTerminos3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondicionesTerminos3(String value) {
        this.condicionesTerminos3 = value;
    }

    /**
     * Obtiene el valor de la propiedad condicionesTerminos4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondicionesTerminos4() {
        return condicionesTerminos4;
    }

    /**
     * Define el valor de la propiedad condicionesTerminos4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondicionesTerminos4(String value) {
        this.condicionesTerminos4 = value;
    }

    /**
     * Obtiene el valor de la propiedad condicionesTerminos5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondicionesTerminos5() {
        return condicionesTerminos5;
    }

    /**
     * Define el valor de la propiedad condicionesTerminos5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondicionesTerminos5(String value) {
        this.condicionesTerminos5 = value;
    }

    /**
     * Obtiene el valor de la propiedad condicionesTerminos6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondicionesTerminos6() {
        return condicionesTerminos6;
    }

    /**
     * Define el valor de la propiedad condicionesTerminos6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondicionesTerminos6(String value) {
        this.condicionesTerminos6 = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Define el valor de la propiedad fechaOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaOperacion(String value) {
        this.fechaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad horaOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraOperacion() {
        return horaOperacion;
    }

    /**
     * Define el valor de la propiedad horaOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraOperacion(String value) {
        this.horaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaValor() {
        return fechaValor;
    }

    /**
     * Define el valor de la propiedad fechaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaValor(String value) {
        this.fechaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad horaValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraValor() {
        return horaValor;
    }

    /**
     * Define el valor de la propiedad horaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraValor(String value) {
        this.horaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCambio.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTipoCambio() {
        return tipoCambio;
    }

    /**
     * Define el valor de la propiedad tipoCambio.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTipoCambio(BigDecimal value) {
        this.tipoCambio = value;
    }

    /**
     * Obtiene el valor de la propiedad monedaSecB1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonedaSecB1() {
        return monedaSecB1;
    }

    /**
     * Define el valor de la propiedad monedaSecB1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonedaSecB1(String value) {
        this.monedaSecB1 = value;
    }

    /**
     * Obtiene el valor de la propiedad montoSecB1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoSecB1() {
        return montoSecB1;
    }

    /**
     * Define el valor de la propiedad montoSecB1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoSecB1(BigDecimal value) {
        this.montoSecB1 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteEntregaSecB1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteEntregaSecB1() {
        return agenteEntregaSecB1;
    }

    /**
     * Define el valor de la propiedad agenteEntregaSecB1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteEntregaSecB1(String value) {
        this.agenteEntregaSecB1 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB11.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB11() {
        return intermediarioSecB11;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB11.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB11(String value) {
        this.intermediarioSecB11 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB12() {
        return intermediarioSecB12;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB12(String value) {
        this.intermediarioSecB12 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB13.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB13() {
        return intermediarioSecB13;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB13.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB13(String value) {
        this.intermediarioSecB13 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB14.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB14() {
        return intermediarioSecB14;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB14.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB14(String value) {
        this.intermediarioSecB14 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB15.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB15() {
        return intermediarioSecB15;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB15.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB15(String value) {
        this.intermediarioSecB15 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB11.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB11() {
        return agenteReceptorSecB11;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB11.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB11(String value) {
        this.agenteReceptorSecB11 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB12() {
        return agenteReceptorSecB12;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB12(String value) {
        this.agenteReceptorSecB12 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB13.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB13() {
        return agenteReceptorSecB13;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB13.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB13(String value) {
        this.agenteReceptorSecB13 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB14.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB14() {
        return agenteReceptorSecB14;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB14.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB14(String value) {
        this.agenteReceptorSecB14 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB15.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB15() {
        return agenteReceptorSecB15;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB15.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB15(String value) {
        this.agenteReceptorSecB15 = value;
    }

    /**
     * Obtiene el valor de la propiedad monedaSecB2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonedaSecB2() {
        return monedaSecB2;
    }

    /**
     * Define el valor de la propiedad monedaSecB2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonedaSecB2(String value) {
        this.monedaSecB2 = value;
    }

    /**
     * Obtiene el valor de la propiedad montoSecB2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoSecB2() {
        return montoSecB2;
    }

    /**
     * Define el valor de la propiedad montoSecB2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoSecB2(BigDecimal value) {
        this.montoSecB2 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteEntregaSecB2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteEntregaSecB2() {
        return agenteEntregaSecB2;
    }

    /**
     * Define el valor de la propiedad agenteEntregaSecB2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteEntregaSecB2(String value) {
        this.agenteEntregaSecB2 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB21.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB21() {
        return intermediarioSecB21;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB21.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB21(String value) {
        this.intermediarioSecB21 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB22.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB22() {
        return intermediarioSecB22;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB22.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB22(String value) {
        this.intermediarioSecB22 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB23.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB23() {
        return intermediarioSecB23;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB23.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB23(String value) {
        this.intermediarioSecB23 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB24.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB24() {
        return intermediarioSecB24;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB24.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB24(String value) {
        this.intermediarioSecB24 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecB25.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecB25() {
        return intermediarioSecB25;
    }

    /**
     * Define el valor de la propiedad intermediarioSecB25.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecB25(String value) {
        this.intermediarioSecB25 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB21.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB21() {
        return agenteReceptorSecB21;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB21.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB21(String value) {
        this.agenteReceptorSecB21 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB22.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB22() {
        return agenteReceptorSecB22;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB22.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB22(String value) {
        this.agenteReceptorSecB22 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB23.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB23() {
        return agenteReceptorSecB23;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB23.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB23(String value) {
        this.agenteReceptorSecB23 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB24.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB24() {
        return agenteReceptorSecB24;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB24.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB24(String value) {
        this.agenteReceptorSecB24 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecB25.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecB25() {
        return agenteReceptorSecB25;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecB25.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecB25(String value) {
        this.agenteReceptorSecB25 = value;
    }

    /**
     * Obtiene el valor de la propiedad institucionBeneficiarioSecB.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitucionBeneficiarioSecB() {
        return institucionBeneficiarioSecB;
    }

    /**
     * Define el valor de la propiedad institucionBeneficiarioSecB.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitucionBeneficiarioSecB(String value) {
        this.institucionBeneficiarioSecB = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionContacto1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionContacto1() {
        return informacionContacto1;
    }

    /**
     * Define el valor de la propiedad informacionContacto1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionContacto1(String value) {
        this.informacionContacto1 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionContacto2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionContacto2() {
        return informacionContacto2;
    }

    /**
     * Define el valor de la propiedad informacionContacto2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionContacto2(String value) {
        this.informacionContacto2 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionContacto3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionContacto3() {
        return informacionContacto3;
    }

    /**
     * Define el valor de la propiedad informacionContacto3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionContacto3(String value) {
        this.informacionContacto3 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionContacto4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionContacto4() {
        return informacionContacto4;
    }

    /**
     * Define el valor de la propiedad informacionContacto4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionContacto4(String value) {
        this.informacionContacto4 = value;
    }

    /**
     * Obtiene el valor de la propiedad metodoTratar1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetodoTratar1() {
        return metodoTratar1;
    }

    /**
     * Define el valor de la propiedad metodoTratar1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetodoTratar1(String value) {
        this.metodoTratar1 = value;
    }

    /**
     * Obtiene el valor de la propiedad metodoTratar2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetodoTratar2() {
        return metodoTratar2;
    }

    /**
     * Define el valor de la propiedad metodoTratar2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetodoTratar2(String value) {
        this.metodoTratar2 = value;
    }

    /**
     * Obtiene el valor de la propiedad metodoTratar3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetodoTratar3() {
        return metodoTratar3;
    }

    /**
     * Define el valor de la propiedad metodoTratar3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetodoTratar3(String value) {
        this.metodoTratar3 = value;
    }

    /**
     * Obtiene el valor de la propiedad metodoTratar4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetodoTratar4() {
        return metodoTratar4;
    }

    /**
     * Define el valor de la propiedad metodoTratar4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetodoTratar4(String value) {
        this.metodoTratar4 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos11.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos11() {
        return tratos11;
    }

    /**
     * Define el valor de la propiedad tratos11.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos11(String value) {
        this.tratos11 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos12() {
        return tratos12;
    }

    /**
     * Define el valor de la propiedad tratos12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos12(String value) {
        this.tratos12 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos13.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos13() {
        return tratos13;
    }

    /**
     * Define el valor de la propiedad tratos13.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos13(String value) {
        this.tratos13 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos14.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos14() {
        return tratos14;
    }

    /**
     * Define el valor de la propiedad tratos14.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos14(String value) {
        this.tratos14 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos15.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos15() {
        return tratos15;
    }

    /**
     * Define el valor de la propiedad tratos15.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos15(String value) {
        this.tratos15 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos21.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos21() {
        return tratos21;
    }

    /**
     * Define el valor de la propiedad tratos21.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos21(String value) {
        this.tratos21 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos22.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos22() {
        return tratos22;
    }

    /**
     * Define el valor de la propiedad tratos22.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos22(String value) {
        this.tratos22 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos23.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos23() {
        return tratos23;
    }

    /**
     * Define el valor de la propiedad tratos23.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos23(String value) {
        this.tratos23 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos24.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos24() {
        return tratos24;
    }

    /**
     * Define el valor de la propiedad tratos24.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos24(String value) {
        this.tratos24 = value;
    }

    /**
     * Obtiene el valor de la propiedad tratos25.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTratos25() {
        return tratos25;
    }

    /**
     * Define el valor de la propiedad tratos25.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTratos25(String value) {
        this.tratos25 = value;
    }

    /**
     * Obtiene el valor de la propiedad idBroker1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdBroker1() {
        return idBroker1;
    }

    /**
     * Define el valor de la propiedad idBroker1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdBroker1(String value) {
        this.idBroker1 = value;
    }

    /**
     * Obtiene el valor de la propiedad idBroker2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdBroker2() {
        return idBroker2;
    }

    /**
     * Define el valor de la propiedad idBroker2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdBroker2(String value) {
        this.idBroker2 = value;
    }

    /**
     * Obtiene el valor de la propiedad idBroker3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdBroker3() {
        return idBroker3;
    }

    /**
     * Define el valor de la propiedad idBroker3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdBroker3(String value) {
        this.idBroker3 = value;
    }

    /**
     * Obtiene el valor de la propiedad idBroker4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdBroker4() {
        return idBroker4;
    }

    /**
     * Define el valor de la propiedad idBroker4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdBroker4(String value) {
        this.idBroker4 = value;
    }

    /**
     * Obtiene el valor de la propiedad idBroker5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdBroker5() {
        return idBroker5;
    }

    /**
     * Define el valor de la propiedad idBroker5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdBroker5(String value) {
        this.idBroker5 = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionBrokerMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComisionBrokerMoneda() {
        return comisionBrokerMoneda;
    }

    /**
     * Define el valor de la propiedad comisionBrokerMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComisionBrokerMoneda(String value) {
        this.comisionBrokerMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionBrokerMonto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComisionBrokerMonto() {
        return comisionBrokerMonto;
    }

    /**
     * Define el valor de la propiedad comisionBrokerMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComisionBrokerMonto(BigDecimal value) {
        this.comisionBrokerMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContraparte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContraparte() {
        return referenciaContraparte;
    }

    /**
     * Define el valor de la propiedad referenciaContraparte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContraparte(String value) {
        this.referenciaContraparte = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaBroker.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaBroker() {
        return referenciaBroker;
    }

    /**
     * Define el valor de la propiedad referenciaBroker.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaBroker(String value) {
        this.referenciaBroker = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente1() {
        return informacionRemitente1;
    }

    /**
     * Define el valor de la propiedad informacionRemitente1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente1(String value) {
        this.informacionRemitente1 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente2() {
        return informacionRemitente2;
    }

    /**
     * Define el valor de la propiedad informacionRemitente2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente2(String value) {
        this.informacionRemitente2 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente3() {
        return informacionRemitente3;
    }

    /**
     * Define el valor de la propiedad informacionRemitente3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente3(String value) {
        this.informacionRemitente3 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente4() {
        return informacionRemitente4;
    }

    /**
     * Define el valor de la propiedad informacionRemitente4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente4(String value) {
        this.informacionRemitente4 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente5() {
        return informacionRemitente5;
    }

    /**
     * Define el valor de la propiedad informacionRemitente5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente5(String value) {
        this.informacionRemitente5 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente6() {
        return informacionRemitente6;
    }

    /**
     * Define el valor de la propiedad informacionRemitente6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente6(String value) {
        this.informacionRemitente6 = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorCompraVenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorCompraVenta() {
        return indicadorCompraVenta;
    }

    /**
     * Define el valor de la propiedad indicadorCompraVenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorCompraVenta(String value) {
        this.indicadorCompraVenta = value;
    }

    /**
     * Obtiene el valor de la propiedad monedaSecD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonedaSecD() {
        return monedaSecD;
    }

    /**
     * Define el valor de la propiedad monedaSecD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonedaSecD(String value) {
        this.monedaSecD = value;
    }

    /**
     * Obtiene el valor de la propiedad montoSecD.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoSecD() {
        return montoSecD;
    }

    /**
     * Define el valor de la propiedad montoSecD.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoSecD(BigDecimal value) {
        this.montoSecD = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteEntregaSecD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteEntregaSecD() {
        return agenteEntregaSecD;
    }

    /**
     * Define el valor de la propiedad agenteEntregaSecD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteEntregaSecD(String value) {
        this.agenteEntregaSecD = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecD1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecD1() {
        return intermediarioSecD1;
    }

    /**
     * Define el valor de la propiedad intermediarioSecD1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecD1(String value) {
        this.intermediarioSecD1 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecD2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecD2() {
        return intermediarioSecD2;
    }

    /**
     * Define el valor de la propiedad intermediarioSecD2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecD2(String value) {
        this.intermediarioSecD2 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecD3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecD3() {
        return intermediarioSecD3;
    }

    /**
     * Define el valor de la propiedad intermediarioSecD3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecD3(String value) {
        this.intermediarioSecD3 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecD4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecD4() {
        return intermediarioSecD4;
    }

    /**
     * Define el valor de la propiedad intermediarioSecD4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecD4(String value) {
        this.intermediarioSecD4 = value;
    }

    /**
     * Obtiene el valor de la propiedad intermediarioSecD5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermediarioSecD5() {
        return intermediarioSecD5;
    }

    /**
     * Define el valor de la propiedad intermediarioSecD5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermediarioSecD5(String value) {
        this.intermediarioSecD5 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecD1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecD1() {
        return agenteReceptorSecD1;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecD1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecD1(String value) {
        this.agenteReceptorSecD1 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecD2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecD2() {
        return agenteReceptorSecD2;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecD2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecD2(String value) {
        this.agenteReceptorSecD2 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecD3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecD3() {
        return agenteReceptorSecD3;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecD3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecD3(String value) {
        this.agenteReceptorSecD3 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecD4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecD4() {
        return agenteReceptorSecD4;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecD4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecD4(String value) {
        this.agenteReceptorSecD4 = value;
    }

    /**
     * Obtiene el valor de la propiedad agenteReceptorSecD5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenteReceptorSecD5() {
        return agenteReceptorSecD5;
    }

    /**
     * Define el valor de la propiedad agenteReceptorSecD5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenteReceptorSecD5(String value) {
        this.agenteReceptorSecD5 = value;
    }

    /**
     * Obtiene el valor de la propiedad institucionBeneficiarioSecD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitucionBeneficiarioSecD() {
        return institucionBeneficiarioSecD;
    }

    /**
     * Define el valor de la propiedad institucionBeneficiarioSecD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitucionBeneficiarioSecD(String value) {
        this.institucionBeneficiarioSecD = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroEstablecimientos.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumeroEstablecimientos() {
        return numeroEstablecimientos;
    }

    /**
     * Define el valor de la propiedad numeroEstablecimientos.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumeroEstablecimientos(BigDecimal value) {
        this.numeroEstablecimientos = value;
    }

}
