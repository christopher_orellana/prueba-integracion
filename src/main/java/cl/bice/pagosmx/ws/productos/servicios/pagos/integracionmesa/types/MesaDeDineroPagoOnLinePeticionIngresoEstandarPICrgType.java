
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para mesaDeDineroPagoOnLinePeticionIngresoEstandarPICrgType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="mesaDeDineroPagoOnLinePeticionIngresoEstandarPICrgType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CodigoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NumeroOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="TipoOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="rutClienteOrdenante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SucursalRutOrdenante" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="NombreClienteOrdenante_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteOrdenante_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteOrdenante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoDcvOrdenante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MontoOperacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="FechaValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="rutClienteBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SucursalRutBeneficiario" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="CodigoDcvBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BancoBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemesa_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemesa_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemesa_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemesa_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="DetalleGastos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HoraValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CantidadContratos" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FormaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorSesion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoPaisCorresponsal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoBancoCorresponsal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteCorresponsal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoPaisIntermediario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoBancoIntermediario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteIntermediario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="UsuarioMDP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="UsuarioIngreso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CargoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SobregiroCtaCte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mesaDeDineroPagoOnLinePeticionIngresoEstandarPICrgType", propOrder = {
    "codigoProducto",
    "numeroOperacion",
    "tipoOperacion",
    "indicadorAccion",
    "fechaOperacion",
    "moneda",
    "rutClienteOrdenante",
    "sucursalRutOrdenante",
    "nombreClienteOrdenante1",
    "nombreClienteOrdenante2",
    "cuentaCorrienteOrdenante",
    "codigoDcvOrdenante",
    "montoOperacion",
    "fechaValor",
    "rutClienteBeneficiario",
    "sucursalRutBeneficiario",
    "codigoDcvBeneficiario",
    "nombreClienteBeneficiario1",
    "nombreClienteBeneficiario2",
    "nombreClienteBeneficiario3",
    "nombreClienteBeneficiario4",
    "bancoBeneficiario",
    "cuentaCorrienteBeneficiario",
    "informacionRemesa1",
    "informacionRemesa2",
    "informacionRemesa3",
    "informacionRemesa4",
    "detalleGastos",
    "horaValor",
    "cantidadContratos",
    "referenciaContrato1",
    "referenciaContrato2",
    "referenciaContrato3",
    "referenciaContrato4",
    "referenciaContrato5",
    "referenciaContrato6",
    "referenciaContrato7",
    "referenciaContrato8",
    "referenciaContrato9",
    "referenciaContrato10",
    "formaPago",
    "indicadorSesion",
    "codigoPaisCorresponsal",
    "codigoBancoCorresponsal",
    "cuentaCorrienteCorresponsal",
    "codigoPaisIntermediario",
    "codigoBancoIntermediario",
    "cuentaCorrienteIntermediario",
    "usuarioMDP",
    "usuarioIngreso",
    "cargoCtaCte",
    "sobregiroCtaCte"
})
public class MesaDeDineroPagoOnLinePeticionIngresoEstandarPICrgType {

    @XmlElement(name = "CodigoProducto")
    protected String codigoProducto;
    @XmlElement(name = "NumeroOperacion")
    protected int numeroOperacion;
    @XmlElement(name = "TipoOperacion")
    protected String tipoOperacion;
    @XmlElement(name = "IndicadorAccion")
    protected String indicadorAccion;
    @XmlElement(name = "FechaOperacion")
    protected String fechaOperacion;
    @XmlElement(name = "Moneda")
    protected String moneda;
    protected String rutClienteOrdenante;
    @XmlElement(name = "SucursalRutOrdenante")
    @XmlSchemaType(name = "unsignedByte")
    protected short sucursalRutOrdenante;
    @XmlElement(name = "NombreClienteOrdenante_1")
    protected String nombreClienteOrdenante1;
    @XmlElement(name = "NombreClienteOrdenante_2")
    protected String nombreClienteOrdenante2;
    @XmlElement(name = "CuentaCorrienteOrdenante")
    protected String cuentaCorrienteOrdenante;
    @XmlElement(name = "CodigoDcvOrdenante")
    protected String codigoDcvOrdenante;
    @XmlElement(name = "MontoOperacion", required = true)
    protected BigDecimal montoOperacion;
    @XmlElement(name = "FechaValor")
    protected String fechaValor;
    protected String rutClienteBeneficiario;
    @XmlElement(name = "SucursalRutBeneficiario")
    @XmlSchemaType(name = "unsignedByte")
    protected short sucursalRutBeneficiario;
    @XmlElement(name = "CodigoDcvBeneficiario")
    protected String codigoDcvBeneficiario;
    @XmlElement(name = "NombreClienteBeneficiario_1")
    protected String nombreClienteBeneficiario1;
    @XmlElement(name = "NombreClienteBeneficiario_2")
    protected String nombreClienteBeneficiario2;
    @XmlElement(name = "NombreClienteBeneficiario_3")
    protected String nombreClienteBeneficiario3;
    @XmlElement(name = "NombreClienteBeneficiario_4")
    protected String nombreClienteBeneficiario4;
    @XmlElement(name = "BancoBeneficiario")
    protected String bancoBeneficiario;
    @XmlElement(name = "CuentaCorrienteBeneficiario")
    protected String cuentaCorrienteBeneficiario;
    @XmlElement(name = "InformacionRemesa_1")
    protected String informacionRemesa1;
    @XmlElement(name = "InformacionRemesa_2")
    protected String informacionRemesa2;
    @XmlElement(name = "InformacionRemesa_3")
    protected String informacionRemesa3;
    @XmlElement(name = "InformacionRemesa_4")
    protected String informacionRemesa4;
    @XmlElement(name = "DetalleGastos")
    protected String detalleGastos;
    @XmlElement(name = "HoraValor")
    protected String horaValor;
    @XmlElement(name = "CantidadContratos")
    @XmlSchemaType(name = "unsignedByte")
    protected short cantidadContratos;
    @XmlElement(name = "ReferenciaContrato_1")
    protected String referenciaContrato1;
    @XmlElement(name = "ReferenciaContrato_2")
    protected String referenciaContrato2;
    @XmlElement(name = "ReferenciaContrato_3")
    protected String referenciaContrato3;
    @XmlElement(name = "ReferenciaContrato_4")
    protected String referenciaContrato4;
    @XmlElement(name = "ReferenciaContrato_5")
    protected String referenciaContrato5;
    @XmlElement(name = "ReferenciaContrato_6")
    protected String referenciaContrato6;
    @XmlElement(name = "ReferenciaContrato_7")
    protected String referenciaContrato7;
    @XmlElement(name = "ReferenciaContrato_8")
    protected String referenciaContrato8;
    @XmlElement(name = "ReferenciaContrato_9")
    protected String referenciaContrato9;
    @XmlElement(name = "ReferenciaContrato_10")
    protected String referenciaContrato10;
    @XmlElement(name = "FormaPago")
    protected String formaPago;
    @XmlElement(name = "IndicadorSesion")
    protected String indicadorSesion;
    @XmlElement(name = "CodigoPaisCorresponsal")
    protected String codigoPaisCorresponsal;
    @XmlElement(name = "CodigoBancoCorresponsal")
    protected String codigoBancoCorresponsal;
    @XmlElement(name = "CuentaCorrienteCorresponsal")
    protected String cuentaCorrienteCorresponsal;
    @XmlElement(name = "CodigoPaisIntermediario")
    protected String codigoPaisIntermediario;
    @XmlElement(name = "CodigoBancoIntermediario")
    protected String codigoBancoIntermediario;
    @XmlElement(name = "CuentaCorrienteIntermediario")
    protected String cuentaCorrienteIntermediario;
    @XmlElement(name = "UsuarioMDP")
    protected String usuarioMDP;
    @XmlElement(name = "UsuarioIngreso")
    protected String usuarioIngreso;
    @XmlElement(name = "CargoCtaCte")
    protected String cargoCtaCte;
    @XmlElement(name = "SobregiroCtaCte")
    protected String sobregiroCtaCte;

    /**
     * Obtiene el valor de la propiedad codigoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Define el valor de la propiedad codigoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProducto(String value) {
        this.codigoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroOperacion.
     * 
     */
    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    /**
     * Define el valor de la propiedad numeroOperacion.
     * 
     */
    public void setNumeroOperacion(int value) {
        this.numeroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * Define el valor de la propiedad tipoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperacion(String value) {
        this.tipoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorAccion() {
        return indicadorAccion;
    }

    /**
     * Define el valor de la propiedad indicadorAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorAccion(String value) {
        this.indicadorAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Define el valor de la propiedad fechaOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaOperacion(String value) {
        this.fechaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad rutClienteOrdenante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutClienteOrdenante() {
        return rutClienteOrdenante;
    }

    /**
     * Define el valor de la propiedad rutClienteOrdenante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutClienteOrdenante(String value) {
        this.rutClienteOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursalRutOrdenante.
     * 
     */
    public short getSucursalRutOrdenante() {
        return sucursalRutOrdenante;
    }

    /**
     * Define el valor de la propiedad sucursalRutOrdenante.
     * 
     */
    public void setSucursalRutOrdenante(short value) {
        this.sucursalRutOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteOrdenante1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteOrdenante1() {
        return nombreClienteOrdenante1;
    }

    /**
     * Define el valor de la propiedad nombreClienteOrdenante1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteOrdenante1(String value) {
        this.nombreClienteOrdenante1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteOrdenante2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteOrdenante2() {
        return nombreClienteOrdenante2;
    }

    /**
     * Define el valor de la propiedad nombreClienteOrdenante2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteOrdenante2(String value) {
        this.nombreClienteOrdenante2 = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteOrdenante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteOrdenante() {
        return cuentaCorrienteOrdenante;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteOrdenante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteOrdenante(String value) {
        this.cuentaCorrienteOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoDcvOrdenante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDcvOrdenante() {
        return codigoDcvOrdenante;
    }

    /**
     * Define el valor de la propiedad codigoDcvOrdenante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDcvOrdenante(String value) {
        this.codigoDcvOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad montoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoOperacion() {
        return montoOperacion;
    }

    /**
     * Define el valor de la propiedad montoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoOperacion(BigDecimal value) {
        this.montoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaValor() {
        return fechaValor;
    }

    /**
     * Define el valor de la propiedad fechaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaValor(String value) {
        this.fechaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad rutClienteBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutClienteBeneficiario() {
        return rutClienteBeneficiario;
    }

    /**
     * Define el valor de la propiedad rutClienteBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutClienteBeneficiario(String value) {
        this.rutClienteBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursalRutBeneficiario.
     * 
     */
    public short getSucursalRutBeneficiario() {
        return sucursalRutBeneficiario;
    }

    /**
     * Define el valor de la propiedad sucursalRutBeneficiario.
     * 
     */
    public void setSucursalRutBeneficiario(short value) {
        this.sucursalRutBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoDcvBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDcvBeneficiario() {
        return codigoDcvBeneficiario;
    }

    /**
     * Define el valor de la propiedad codigoDcvBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDcvBeneficiario(String value) {
        this.codigoDcvBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario1() {
        return nombreClienteBeneficiario1;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario1(String value) {
        this.nombreClienteBeneficiario1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario2() {
        return nombreClienteBeneficiario2;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario2(String value) {
        this.nombreClienteBeneficiario2 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario3() {
        return nombreClienteBeneficiario3;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario3(String value) {
        this.nombreClienteBeneficiario3 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario4() {
        return nombreClienteBeneficiario4;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario4(String value) {
        this.nombreClienteBeneficiario4 = value;
    }

    /**
     * Obtiene el valor de la propiedad bancoBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBancoBeneficiario() {
        return bancoBeneficiario;
    }

    /**
     * Define el valor de la propiedad bancoBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBancoBeneficiario(String value) {
        this.bancoBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteBeneficiario() {
        return cuentaCorrienteBeneficiario;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteBeneficiario(String value) {
        this.cuentaCorrienteBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemesa1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemesa1() {
        return informacionRemesa1;
    }

    /**
     * Define el valor de la propiedad informacionRemesa1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemesa1(String value) {
        this.informacionRemesa1 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemesa2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemesa2() {
        return informacionRemesa2;
    }

    /**
     * Define el valor de la propiedad informacionRemesa2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemesa2(String value) {
        this.informacionRemesa2 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemesa3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemesa3() {
        return informacionRemesa3;
    }

    /**
     * Define el valor de la propiedad informacionRemesa3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemesa3(String value) {
        this.informacionRemesa3 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemesa4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemesa4() {
        return informacionRemesa4;
    }

    /**
     * Define el valor de la propiedad informacionRemesa4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemesa4(String value) {
        this.informacionRemesa4 = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleGastos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetalleGastos() {
        return detalleGastos;
    }

    /**
     * Define el valor de la propiedad detalleGastos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetalleGastos(String value) {
        this.detalleGastos = value;
    }

    /**
     * Obtiene el valor de la propiedad horaValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraValor() {
        return horaValor;
    }

    /**
     * Define el valor de la propiedad horaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraValor(String value) {
        this.horaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidadContratos.
     * 
     */
    public short getCantidadContratos() {
        return cantidadContratos;
    }

    /**
     * Define el valor de la propiedad cantidadContratos.
     * 
     */
    public void setCantidadContratos(short value) {
        this.cantidadContratos = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato1() {
        return referenciaContrato1;
    }

    /**
     * Define el valor de la propiedad referenciaContrato1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato1(String value) {
        this.referenciaContrato1 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato2() {
        return referenciaContrato2;
    }

    /**
     * Define el valor de la propiedad referenciaContrato2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato2(String value) {
        this.referenciaContrato2 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato3() {
        return referenciaContrato3;
    }

    /**
     * Define el valor de la propiedad referenciaContrato3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato3(String value) {
        this.referenciaContrato3 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato4() {
        return referenciaContrato4;
    }

    /**
     * Define el valor de la propiedad referenciaContrato4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato4(String value) {
        this.referenciaContrato4 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato5() {
        return referenciaContrato5;
    }

    /**
     * Define el valor de la propiedad referenciaContrato5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato5(String value) {
        this.referenciaContrato5 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato6() {
        return referenciaContrato6;
    }

    /**
     * Define el valor de la propiedad referenciaContrato6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato6(String value) {
        this.referenciaContrato6 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato7.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato7() {
        return referenciaContrato7;
    }

    /**
     * Define el valor de la propiedad referenciaContrato7.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato7(String value) {
        this.referenciaContrato7 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato8.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato8() {
        return referenciaContrato8;
    }

    /**
     * Define el valor de la propiedad referenciaContrato8.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato8(String value) {
        this.referenciaContrato8 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato9.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato9() {
        return referenciaContrato9;
    }

    /**
     * Define el valor de la propiedad referenciaContrato9.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato9(String value) {
        this.referenciaContrato9 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato10() {
        return referenciaContrato10;
    }

    /**
     * Define el valor de la propiedad referenciaContrato10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato10(String value) {
        this.referenciaContrato10 = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * Define el valor de la propiedad formaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPago(String value) {
        this.formaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorSesion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorSesion() {
        return indicadorSesion;
    }

    /**
     * Define el valor de la propiedad indicadorSesion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorSesion(String value) {
        this.indicadorSesion = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoPaisCorresponsal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPaisCorresponsal() {
        return codigoPaisCorresponsal;
    }

    /**
     * Define el valor de la propiedad codigoPaisCorresponsal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPaisCorresponsal(String value) {
        this.codigoPaisCorresponsal = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoBancoCorresponsal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoBancoCorresponsal() {
        return codigoBancoCorresponsal;
    }

    /**
     * Define el valor de la propiedad codigoBancoCorresponsal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoBancoCorresponsal(String value) {
        this.codigoBancoCorresponsal = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteCorresponsal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteCorresponsal() {
        return cuentaCorrienteCorresponsal;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteCorresponsal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteCorresponsal(String value) {
        this.cuentaCorrienteCorresponsal = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoPaisIntermediario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPaisIntermediario() {
        return codigoPaisIntermediario;
    }

    /**
     * Define el valor de la propiedad codigoPaisIntermediario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPaisIntermediario(String value) {
        this.codigoPaisIntermediario = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoBancoIntermediario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoBancoIntermediario() {
        return codigoBancoIntermediario;
    }

    /**
     * Define el valor de la propiedad codigoBancoIntermediario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoBancoIntermediario(String value) {
        this.codigoBancoIntermediario = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteIntermediario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteIntermediario() {
        return cuentaCorrienteIntermediario;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteIntermediario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteIntermediario(String value) {
        this.cuentaCorrienteIntermediario = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioMDP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioMDP() {
        return usuarioMDP;
    }

    /**
     * Define el valor de la propiedad usuarioMDP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioMDP(String value) {
        this.usuarioMDP = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioIngreso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioIngreso() {
        return usuarioIngreso;
    }

    /**
     * Define el valor de la propiedad usuarioIngreso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioIngreso(String value) {
        this.usuarioIngreso = value;
    }

    /**
     * Obtiene el valor de la propiedad cargoCtaCte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargoCtaCte() {
        return cargoCtaCte;
    }

    /**
     * Define el valor de la propiedad cargoCtaCte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargoCtaCte(String value) {
        this.cargoCtaCte = value;
    }

    /**
     * Obtiene el valor de la propiedad sobregiroCtaCte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSobregiroCtaCte() {
        return sobregiroCtaCte;
    }

    /**
     * Define el valor de la propiedad sobregiroCtaCte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSobregiroCtaCte(String value) {
        this.sobregiroCtaCte = value;
    }

}
