
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para mesaDeDineroPagoOnLinePeticionIngresoLegacyType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="mesaDeDineroPagoOnLinePeticionIngresoLegacyType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CodigoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NumeroOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="TipoOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="rutClienteOrdenante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SucursalRutOrdenante" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="NombreClienteOrdenante_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteOrdenante_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteOrdenante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MontoOperacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="FechaValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="rutClienteBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SucursalRutBeneficiario" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="NombreClienteRutBeneficiario_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteRutBeneficiario_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteRutBeneficiario_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteRutBeneficiario_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BancoBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemitente_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRad_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRad_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRad_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRad_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRad_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRad_6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="DetalleGastos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HoraValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FormaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BancoCorresponsal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteCorresponsal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BancoIntermediario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteIntermediario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreIntermediario_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreIntermediario_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreIntermediario_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreIntermediario_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MontoCrgMN" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MonedaCrgMN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteCrgMN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MontoCrgMX" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MonedaCrgMX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteCrgMX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="UsuarioMDP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="UsuarioIngreso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CargoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SobregiroCtaCte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ArchivoLegacy" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mesaDeDineroPagoOnLinePeticionIngresoLegacyType", propOrder = {
    "codigoProducto",
    "numeroOperacion",
    "tipoOperacion",
    "indicadorAccion",
    "fechaOperacion",
    "moneda",
    "rutClienteOrdenante",
    "sucursalRutOrdenante",
    "nombreClienteOrdenante1",
    "nombreClienteOrdenante2",
    "cuentaCorrienteOrdenante",
    "montoOperacion",
    "fechaValor",
    "rutClienteBeneficiario",
    "sucursalRutBeneficiario",
    "nombreClienteRutBeneficiario1",
    "nombreClienteRutBeneficiario2",
    "nombreClienteRutBeneficiario3",
    "nombreClienteRutBeneficiario4",
    "nombreClienteBeneficiario",
    "nombreClienteBeneficiario1",
    "nombreClienteBeneficiario2",
    "nombreClienteBeneficiario3",
    "nombreClienteBeneficiario4",
    "bancoBeneficiario",
    "cuentaCorrienteBeneficiario",
    "informacionRemitente1",
    "informacionRemitente2",
    "informacionRemitente3",
    "informacionRemitente4",
    "informacionRad1",
    "informacionRad2",
    "informacionRad3",
    "informacionRad4",
    "informacionRad5",
    "informacionRad6",
    "detalleGastos",
    "horaValor",
    "formaPago",
    "bancoCorresponsal",
    "cuentaCorrienteCorresponsal",
    "bancoIntermediario",
    "cuentaCorrienteIntermediario",
    "nombreIntermediario1",
    "nombreIntermediario2",
    "nombreIntermediario3",
    "nombreIntermediario4",
    "montoCrgMN",
    "monedaCrgMN",
    "cuentaCorrienteCrgMN",
    "montoCrgMX",
    "monedaCrgMX",
    "cuentaCorrienteCrgMX",
    "usuarioMDP",
    "usuarioIngreso",
    "cargoCtaCte",
    "sobregiroCtaCte",
    "nombreArchivo",
    "archivoLegacy"
})
public class MesaDeDineroPagoOnLinePeticionIngresoLegacyType {

    @XmlElement(name = "CodigoProducto")
    protected String codigoProducto;
    @XmlElement(name = "NumeroOperacion")
    protected int numeroOperacion;
    @XmlElement(name = "TipoOperacion")
    protected String tipoOperacion;
    @XmlElement(name = "IndicadorAccion")
    protected String indicadorAccion;
    @XmlElement(name = "FechaOperacion")
    protected String fechaOperacion;
    @XmlElement(name = "Moneda")
    protected String moneda;
    protected String rutClienteOrdenante;
    @XmlElement(name = "SucursalRutOrdenante")
    @XmlSchemaType(name = "unsignedByte")
    protected short sucursalRutOrdenante;
    @XmlElement(name = "NombreClienteOrdenante_1")
    protected String nombreClienteOrdenante1;
    @XmlElement(name = "NombreClienteOrdenante_2")
    protected String nombreClienteOrdenante2;
    @XmlElement(name = "CuentaCorrienteOrdenante")
    protected String cuentaCorrienteOrdenante;
    @XmlElement(name = "MontoOperacion", required = true)
    protected BigDecimal montoOperacion;
    @XmlElement(name = "FechaValor")
    protected String fechaValor;
    protected String rutClienteBeneficiario;
    @XmlElement(name = "SucursalRutBeneficiario")
    @XmlSchemaType(name = "unsignedByte")
    protected short sucursalRutBeneficiario;
    @XmlElement(name = "NombreClienteRutBeneficiario_1")
    protected String nombreClienteRutBeneficiario1;
    @XmlElement(name = "NombreClienteRutBeneficiario_2")
    protected String nombreClienteRutBeneficiario2;
    @XmlElement(name = "NombreClienteRutBeneficiario_3")
    protected String nombreClienteRutBeneficiario3;
    @XmlElement(name = "NombreClienteRutBeneficiario_4")
    protected String nombreClienteRutBeneficiario4;
    @XmlElement(name = "NombreClienteBeneficiario")
    protected String nombreClienteBeneficiario;
    @XmlElement(name = "NombreClienteBeneficiario_1")
    protected String nombreClienteBeneficiario1;
    @XmlElement(name = "NombreClienteBeneficiario_2")
    protected String nombreClienteBeneficiario2;
    @XmlElement(name = "NombreClienteBeneficiario_3")
    protected String nombreClienteBeneficiario3;
    @XmlElement(name = "NombreClienteBeneficiario_4")
    protected String nombreClienteBeneficiario4;
    @XmlElement(name = "BancoBeneficiario")
    protected String bancoBeneficiario;
    @XmlElement(name = "CuentaCorrienteBeneficiario")
    protected String cuentaCorrienteBeneficiario;
    @XmlElement(name = "InformacionRemitente_1")
    protected String informacionRemitente1;
    @XmlElement(name = "InformacionRemitente_2")
    protected String informacionRemitente2;
    @XmlElement(name = "InformacionRemitente_3")
    protected String informacionRemitente3;
    @XmlElement(name = "InformacionRemitente_4")
    protected String informacionRemitente4;
    @XmlElement(name = "InformacionRad_1")
    protected String informacionRad1;
    @XmlElement(name = "InformacionRad_2")
    protected String informacionRad2;
    @XmlElement(name = "InformacionRad_3")
    protected String informacionRad3;
    @XmlElement(name = "InformacionRad_4")
    protected String informacionRad4;
    @XmlElement(name = "InformacionRad_5")
    protected String informacionRad5;
    @XmlElement(name = "InformacionRad_6")
    protected String informacionRad6;
    @XmlElement(name = "DetalleGastos")
    protected String detalleGastos;
    @XmlElement(name = "HoraValor")
    protected String horaValor;
    @XmlElement(name = "FormaPago")
    protected String formaPago;
    @XmlElement(name = "BancoCorresponsal")
    protected String bancoCorresponsal;
    @XmlElement(name = "CuentaCorrienteCorresponsal")
    protected String cuentaCorrienteCorresponsal;
    @XmlElement(name = "BancoIntermediario")
    protected String bancoIntermediario;
    @XmlElement(name = "CuentaCorrienteIntermediario")
    protected String cuentaCorrienteIntermediario;
    @XmlElement(name = "NombreIntermediario_1")
    protected String nombreIntermediario1;
    @XmlElement(name = "NombreIntermediario_2")
    protected String nombreIntermediario2;
    @XmlElement(name = "NombreIntermediario_3")
    protected String nombreIntermediario3;
    @XmlElement(name = "NombreIntermediario_4")
    protected String nombreIntermediario4;
    @XmlElement(name = "MontoCrgMN", required = true)
    protected BigDecimal montoCrgMN;
    @XmlElement(name = "MonedaCrgMN")
    protected String monedaCrgMN;
    @XmlElement(name = "CuentaCorrienteCrgMN")
    protected String cuentaCorrienteCrgMN;
    @XmlElement(name = "MontoCrgMX", required = true)
    protected BigDecimal montoCrgMX;
    @XmlElement(name = "MonedaCrgMX")
    protected String monedaCrgMX;
    @XmlElement(name = "CuentaCorrienteCrgMX")
    protected String cuentaCorrienteCrgMX;
    @XmlElement(name = "UsuarioMDP")
    protected String usuarioMDP;
    @XmlElement(name = "UsuarioIngreso")
    protected String usuarioIngreso;
    @XmlElement(name = "CargoCtaCte")
    protected String cargoCtaCte;
    @XmlElement(name = "SobregiroCtaCte")
    protected String sobregiroCtaCte;
    @XmlElement(name = "NombreArchivo")
    protected String nombreArchivo;
    @XmlElement(name = "ArchivoLegacy")
    protected byte[] archivoLegacy;

    /**
     * Obtiene el valor de la propiedad codigoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Define el valor de la propiedad codigoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProducto(String value) {
        this.codigoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroOperacion.
     * 
     */
    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    /**
     * Define el valor de la propiedad numeroOperacion.
     * 
     */
    public void setNumeroOperacion(int value) {
        this.numeroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * Define el valor de la propiedad tipoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperacion(String value) {
        this.tipoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorAccion() {
        return indicadorAccion;
    }

    /**
     * Define el valor de la propiedad indicadorAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorAccion(String value) {
        this.indicadorAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Define el valor de la propiedad fechaOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaOperacion(String value) {
        this.fechaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad rutClienteOrdenante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutClienteOrdenante() {
        return rutClienteOrdenante;
    }

    /**
     * Define el valor de la propiedad rutClienteOrdenante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutClienteOrdenante(String value) {
        this.rutClienteOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursalRutOrdenante.
     * 
     */
    public short getSucursalRutOrdenante() {
        return sucursalRutOrdenante;
    }

    /**
     * Define el valor de la propiedad sucursalRutOrdenante.
     * 
     */
    public void setSucursalRutOrdenante(short value) {
        this.sucursalRutOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteOrdenante1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteOrdenante1() {
        return nombreClienteOrdenante1;
    }

    /**
     * Define el valor de la propiedad nombreClienteOrdenante1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteOrdenante1(String value) {
        this.nombreClienteOrdenante1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteOrdenante2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteOrdenante2() {
        return nombreClienteOrdenante2;
    }

    /**
     * Define el valor de la propiedad nombreClienteOrdenante2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteOrdenante2(String value) {
        this.nombreClienteOrdenante2 = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteOrdenante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteOrdenante() {
        return cuentaCorrienteOrdenante;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteOrdenante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteOrdenante(String value) {
        this.cuentaCorrienteOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad montoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoOperacion() {
        return montoOperacion;
    }

    /**
     * Define el valor de la propiedad montoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoOperacion(BigDecimal value) {
        this.montoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaValor() {
        return fechaValor;
    }

    /**
     * Define el valor de la propiedad fechaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaValor(String value) {
        this.fechaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad rutClienteBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutClienteBeneficiario() {
        return rutClienteBeneficiario;
    }

    /**
     * Define el valor de la propiedad rutClienteBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutClienteBeneficiario(String value) {
        this.rutClienteBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursalRutBeneficiario.
     * 
     */
    public short getSucursalRutBeneficiario() {
        return sucursalRutBeneficiario;
    }

    /**
     * Define el valor de la propiedad sucursalRutBeneficiario.
     * 
     */
    public void setSucursalRutBeneficiario(short value) {
        this.sucursalRutBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteRutBeneficiario1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteRutBeneficiario1() {
        return nombreClienteRutBeneficiario1;
    }

    /**
     * Define el valor de la propiedad nombreClienteRutBeneficiario1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteRutBeneficiario1(String value) {
        this.nombreClienteRutBeneficiario1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteRutBeneficiario2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteRutBeneficiario2() {
        return nombreClienteRutBeneficiario2;
    }

    /**
     * Define el valor de la propiedad nombreClienteRutBeneficiario2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteRutBeneficiario2(String value) {
        this.nombreClienteRutBeneficiario2 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteRutBeneficiario3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteRutBeneficiario3() {
        return nombreClienteRutBeneficiario3;
    }

    /**
     * Define el valor de la propiedad nombreClienteRutBeneficiario3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteRutBeneficiario3(String value) {
        this.nombreClienteRutBeneficiario3 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteRutBeneficiario4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteRutBeneficiario4() {
        return nombreClienteRutBeneficiario4;
    }

    /**
     * Define el valor de la propiedad nombreClienteRutBeneficiario4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteRutBeneficiario4(String value) {
        this.nombreClienteRutBeneficiario4 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario() {
        return nombreClienteBeneficiario;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario(String value) {
        this.nombreClienteBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario1() {
        return nombreClienteBeneficiario1;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario1(String value) {
        this.nombreClienteBeneficiario1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario2() {
        return nombreClienteBeneficiario2;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario2(String value) {
        this.nombreClienteBeneficiario2 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario3() {
        return nombreClienteBeneficiario3;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario3(String value) {
        this.nombreClienteBeneficiario3 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario4() {
        return nombreClienteBeneficiario4;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario4(String value) {
        this.nombreClienteBeneficiario4 = value;
    }

    /**
     * Obtiene el valor de la propiedad bancoBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBancoBeneficiario() {
        return bancoBeneficiario;
    }

    /**
     * Define el valor de la propiedad bancoBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBancoBeneficiario(String value) {
        this.bancoBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteBeneficiario() {
        return cuentaCorrienteBeneficiario;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteBeneficiario(String value) {
        this.cuentaCorrienteBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente1() {
        return informacionRemitente1;
    }

    /**
     * Define el valor de la propiedad informacionRemitente1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente1(String value) {
        this.informacionRemitente1 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente2() {
        return informacionRemitente2;
    }

    /**
     * Define el valor de la propiedad informacionRemitente2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente2(String value) {
        this.informacionRemitente2 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente3() {
        return informacionRemitente3;
    }

    /**
     * Define el valor de la propiedad informacionRemitente3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente3(String value) {
        this.informacionRemitente3 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemitente4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemitente4() {
        return informacionRemitente4;
    }

    /**
     * Define el valor de la propiedad informacionRemitente4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemitente4(String value) {
        this.informacionRemitente4 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRad1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRad1() {
        return informacionRad1;
    }

    /**
     * Define el valor de la propiedad informacionRad1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRad1(String value) {
        this.informacionRad1 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRad2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRad2() {
        return informacionRad2;
    }

    /**
     * Define el valor de la propiedad informacionRad2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRad2(String value) {
        this.informacionRad2 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRad3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRad3() {
        return informacionRad3;
    }

    /**
     * Define el valor de la propiedad informacionRad3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRad3(String value) {
        this.informacionRad3 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRad4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRad4() {
        return informacionRad4;
    }

    /**
     * Define el valor de la propiedad informacionRad4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRad4(String value) {
        this.informacionRad4 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRad5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRad5() {
        return informacionRad5;
    }

    /**
     * Define el valor de la propiedad informacionRad5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRad5(String value) {
        this.informacionRad5 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRad6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRad6() {
        return informacionRad6;
    }

    /**
     * Define el valor de la propiedad informacionRad6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRad6(String value) {
        this.informacionRad6 = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleGastos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetalleGastos() {
        return detalleGastos;
    }

    /**
     * Define el valor de la propiedad detalleGastos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetalleGastos(String value) {
        this.detalleGastos = value;
    }

    /**
     * Obtiene el valor de la propiedad horaValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraValor() {
        return horaValor;
    }

    /**
     * Define el valor de la propiedad horaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraValor(String value) {
        this.horaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * Define el valor de la propiedad formaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPago(String value) {
        this.formaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad bancoCorresponsal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBancoCorresponsal() {
        return bancoCorresponsal;
    }

    /**
     * Define el valor de la propiedad bancoCorresponsal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBancoCorresponsal(String value) {
        this.bancoCorresponsal = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteCorresponsal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteCorresponsal() {
        return cuentaCorrienteCorresponsal;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteCorresponsal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteCorresponsal(String value) {
        this.cuentaCorrienteCorresponsal = value;
    }

    /**
     * Obtiene el valor de la propiedad bancoIntermediario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBancoIntermediario() {
        return bancoIntermediario;
    }

    /**
     * Define el valor de la propiedad bancoIntermediario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBancoIntermediario(String value) {
        this.bancoIntermediario = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteIntermediario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteIntermediario() {
        return cuentaCorrienteIntermediario;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteIntermediario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteIntermediario(String value) {
        this.cuentaCorrienteIntermediario = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreIntermediario1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreIntermediario1() {
        return nombreIntermediario1;
    }

    /**
     * Define el valor de la propiedad nombreIntermediario1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreIntermediario1(String value) {
        this.nombreIntermediario1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreIntermediario2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreIntermediario2() {
        return nombreIntermediario2;
    }

    /**
     * Define el valor de la propiedad nombreIntermediario2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreIntermediario2(String value) {
        this.nombreIntermediario2 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreIntermediario3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreIntermediario3() {
        return nombreIntermediario3;
    }

    /**
     * Define el valor de la propiedad nombreIntermediario3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreIntermediario3(String value) {
        this.nombreIntermediario3 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreIntermediario4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreIntermediario4() {
        return nombreIntermediario4;
    }

    /**
     * Define el valor de la propiedad nombreIntermediario4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreIntermediario4(String value) {
        this.nombreIntermediario4 = value;
    }

    /**
     * Obtiene el valor de la propiedad montoCrgMN.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoCrgMN() {
        return montoCrgMN;
    }

    /**
     * Define el valor de la propiedad montoCrgMN.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoCrgMN(BigDecimal value) {
        this.montoCrgMN = value;
    }

    /**
     * Obtiene el valor de la propiedad monedaCrgMN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonedaCrgMN() {
        return monedaCrgMN;
    }

    /**
     * Define el valor de la propiedad monedaCrgMN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonedaCrgMN(String value) {
        this.monedaCrgMN = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteCrgMN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteCrgMN() {
        return cuentaCorrienteCrgMN;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteCrgMN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteCrgMN(String value) {
        this.cuentaCorrienteCrgMN = value;
    }

    /**
     * Obtiene el valor de la propiedad montoCrgMX.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoCrgMX() {
        return montoCrgMX;
    }

    /**
     * Define el valor de la propiedad montoCrgMX.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoCrgMX(BigDecimal value) {
        this.montoCrgMX = value;
    }

    /**
     * Obtiene el valor de la propiedad monedaCrgMX.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonedaCrgMX() {
        return monedaCrgMX;
    }

    /**
     * Define el valor de la propiedad monedaCrgMX.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonedaCrgMX(String value) {
        this.monedaCrgMX = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteCrgMX.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteCrgMX() {
        return cuentaCorrienteCrgMX;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteCrgMX.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteCrgMX(String value) {
        this.cuentaCorrienteCrgMX = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioMDP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioMDP() {
        return usuarioMDP;
    }

    /**
     * Define el valor de la propiedad usuarioMDP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioMDP(String value) {
        this.usuarioMDP = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioIngreso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioIngreso() {
        return usuarioIngreso;
    }

    /**
     * Define el valor de la propiedad usuarioIngreso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioIngreso(String value) {
        this.usuarioIngreso = value;
    }

    /**
     * Obtiene el valor de la propiedad cargoCtaCte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargoCtaCte() {
        return cargoCtaCte;
    }

    /**
     * Define el valor de la propiedad cargoCtaCte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargoCtaCte(String value) {
        this.cargoCtaCte = value;
    }

    /**
     * Obtiene el valor de la propiedad sobregiroCtaCte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSobregiroCtaCte() {
        return sobregiroCtaCte;
    }

    /**
     * Define el valor de la propiedad sobregiroCtaCte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSobregiroCtaCte(String value) {
        this.sobregiroCtaCte = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * Define el valor de la propiedad nombreArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreArchivo(String value) {
        this.nombreArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad archivoLegacy.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getArchivoLegacy() {
        return archivoLegacy;
    }

    /**
     * Define el valor de la propiedad archivoLegacy.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setArchivoLegacy(byte[] value) {
        this.archivoLegacy = value;
    }

}
