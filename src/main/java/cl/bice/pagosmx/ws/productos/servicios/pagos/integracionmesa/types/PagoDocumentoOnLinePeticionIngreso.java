
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PagoDocumentoOnLinePeticionIngreso complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PagoDocumentoOnLinePeticionIngreso"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="pagosDocumentos" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}mesaDeDineroPagoDocumentoOnLinePeticionIngresoType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagoDocumentoOnLinePeticionIngreso", propOrder = {
    "pagosDocumentos"
})
public class PagoDocumentoOnLinePeticionIngreso {

    @XmlElement(required = true, nillable = true)
    protected MesaDeDineroPagoDocumentoOnLinePeticionIngresoType pagosDocumentos;

    /**
     * Obtiene el valor de la propiedad pagosDocumentos.
     * 
     * @return
     *     possible object is
     *     {@link MesaDeDineroPagoDocumentoOnLinePeticionIngresoType }
     *     
     */
    public MesaDeDineroPagoDocumentoOnLinePeticionIngresoType getPagosDocumentos() {
        return pagosDocumentos;
    }

    /**
     * Define el valor de la propiedad pagosDocumentos.
     * 
     * @param value
     *     allowed object is
     *     {@link MesaDeDineroPagoDocumentoOnLinePeticionIngresoType }
     *     
     */
    public void setPagosDocumentos(MesaDeDineroPagoDocumentoOnLinePeticionIngresoType value) {
        this.pagosDocumentos = value;
    }

}
