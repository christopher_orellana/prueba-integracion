
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PagoOnLinePeticionIngresoEstandarCrg complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PagoOnLinePeticionIngresoEstandarCrg"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="pagos" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}mesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagoOnLinePeticionIngresoEstandarCrg", propOrder = {
    "pagos"
})
public class PagoOnLinePeticionIngresoEstandarCrg {

    @XmlElement(required = true, nillable = true)
    protected MesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType pagos;

    /**
     * Obtiene el valor de la propiedad pagos.
     * 
     * @return
     *     possible object is
     *     {@link MesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType }
     *     
     */
    public MesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType getPagos() {
        return pagos;
    }

    /**
     * Define el valor de la propiedad pagos.
     * 
     * @param value
     *     allowed object is
     *     {@link MesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType }
     *     
     */
    public void setPagos(MesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType value) {
        this.pagos = value;
    }

}
