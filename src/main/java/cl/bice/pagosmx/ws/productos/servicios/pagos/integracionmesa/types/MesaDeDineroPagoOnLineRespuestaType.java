
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para mesaDeDineroPagoOnLineRespuestaType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="mesaDeDineroPagoOnLineRespuestaType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="IndicadorResultadoOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="MensajeResultadoOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NumeroOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="DetalleMensajesOperacion" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}DetalleMensajesOperacionType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mesaDeDineroPagoOnLineRespuestaType", propOrder = {
    "indicadorResultadoOperacion",
    "mensajeResultadoOperacion",
    "numeroOperacion",
    "detalleMensajesOperacion"
})
public class MesaDeDineroPagoOnLineRespuestaType {

    @XmlElement(name = "IndicadorResultadoOperacion")
    protected int indicadorResultadoOperacion;
    @XmlElement(name = "MensajeResultadoOperacion")
    protected String mensajeResultadoOperacion;
    @XmlElement(name = "NumeroOperacion")
    protected int numeroOperacion;
    @XmlElement(name = "DetalleMensajesOperacion")
    protected DetalleMensajesOperacionType detalleMensajesOperacion;

    /**
     * Obtiene el valor de la propiedad indicadorResultadoOperacion.
     * 
     */
    public int getIndicadorResultadoOperacion() {
        return indicadorResultadoOperacion;
    }

    /**
     * Define el valor de la propiedad indicadorResultadoOperacion.
     * 
     */
    public void setIndicadorResultadoOperacion(int value) {
        this.indicadorResultadoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad mensajeResultadoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeResultadoOperacion() {
        return mensajeResultadoOperacion;
    }

    /**
     * Define el valor de la propiedad mensajeResultadoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeResultadoOperacion(String value) {
        this.mensajeResultadoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroOperacion.
     * 
     */
    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    /**
     * Define el valor de la propiedad numeroOperacion.
     * 
     */
    public void setNumeroOperacion(int value) {
        this.numeroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleMensajesOperacion.
     * 
     * @return
     *     possible object is
     *     {@link DetalleMensajesOperacionType }
     *     
     */
    public DetalleMensajesOperacionType getDetalleMensajesOperacion() {
        return detalleMensajesOperacion;
    }

    /**
     * Define el valor de la propiedad detalleMensajesOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link DetalleMensajesOperacionType }
     *     
     */
    public void setDetalleMensajesOperacion(DetalleMensajesOperacionType value) {
        this.detalleMensajesOperacion = value;
    }

}
