
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoDocumentoOnLinePeticionIngreso;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionAnular;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionAnularCrg;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionAnularPI;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionConfirmarMX;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionConsultar;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionConsultarCrg;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionIngreso;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionIngresoEstandar;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionIngresoEstandarCrg;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionIngresoEstandarPICrg;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionIngresoLegacy;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionIngresoMesaCrg;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionIngresoMesaPICrg;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionIngresoPI;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLineRespuesta;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EnvioPagoOnLinePeticionIngreso_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionIngreso");
    private final static QName _EnvioPagoOnLineRespuesta_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLineRespuesta");
    private final static QName _EnvioPagoDocumentoOnLinePeticionIngreso_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoDocumentoOnLinePeticionIngreso");
    private final static QName _EnvioPagoOnLinePeticionAnular_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionAnular");
    private final static QName _EnvioPagoOnLinePeticionConsultar_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionConsultar");
    private final static QName _EnvioPagoOnLinePeticionIngresoPI_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionIngresoPI");
    private final static QName _EnvioPagoOnLinePeticionAnularPI_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionAnularPI");
    private final static QName _EnvioPagoOnLinePeticionConfirmarMX_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionConfirmarMX");
    private final static QName _EnvioPagoOnLinePeticionIngresoEstandar_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionIngresoEstandar");
    private final static QName _EnvioPagoOnLinePeticionAnularCrg_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionAnularCrg");
    private final static QName _EnvioPagoOnLinePeticionConsultarCrg_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionConsultarCrg");
    private final static QName _EnvioPagoOnLinePeticionIngresoEstandarCrg_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionIngresoEstandarCrg");
    private final static QName _EnvioPagoOnLinePeticionIngresoMesaCrg_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionIngresoMesaCrg");
    private final static QName _EnvioPagoOnLinePeticionIngresoEstandarPICrg_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionIngresoEstandarPICrg");
    private final static QName _EnvioPagoOnLinePeticionIngresoMesaPICrg_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionIngresoMesaPICrg");
    private final static QName _EnvioPagoOnLinePeticionIngresoLegacy_QNAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa", "envioPagoOnLinePeticionIngresoLegacy");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngreso }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngreso }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionIngreso")
    public JAXBElement<PagoOnLinePeticionIngreso> createEnvioPagoOnLinePeticionIngreso(PagoOnLinePeticionIngreso value) {
        return new JAXBElement<PagoOnLinePeticionIngreso>(_EnvioPagoOnLinePeticionIngreso_QNAME, PagoOnLinePeticionIngreso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLineRespuesta }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLineRespuesta }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLineRespuesta")
    public JAXBElement<PagoOnLineRespuesta> createEnvioPagoOnLineRespuesta(PagoOnLineRespuesta value) {
        return new JAXBElement<PagoOnLineRespuesta>(_EnvioPagoOnLineRespuesta_QNAME, PagoOnLineRespuesta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoDocumentoOnLinePeticionIngreso }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoDocumentoOnLinePeticionIngreso }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoDocumentoOnLinePeticionIngreso")
    public JAXBElement<PagoDocumentoOnLinePeticionIngreso> createEnvioPagoDocumentoOnLinePeticionIngreso(PagoDocumentoOnLinePeticionIngreso value) {
        return new JAXBElement<PagoDocumentoOnLinePeticionIngreso>(_EnvioPagoDocumentoOnLinePeticionIngreso_QNAME, PagoDocumentoOnLinePeticionIngreso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionAnular }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionAnular }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionAnular")
    public JAXBElement<PagoOnLinePeticionAnular> createEnvioPagoOnLinePeticionAnular(PagoOnLinePeticionAnular value) {
        return new JAXBElement<PagoOnLinePeticionAnular>(_EnvioPagoOnLinePeticionAnular_QNAME, PagoOnLinePeticionAnular.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionConsultar }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionConsultar }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionConsultar")
    public JAXBElement<PagoOnLinePeticionConsultar> createEnvioPagoOnLinePeticionConsultar(PagoOnLinePeticionConsultar value) {
        return new JAXBElement<PagoOnLinePeticionConsultar>(_EnvioPagoOnLinePeticionConsultar_QNAME, PagoOnLinePeticionConsultar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoPI }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoPI }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionIngresoPI")
    public JAXBElement<PagoOnLinePeticionIngresoPI> createEnvioPagoOnLinePeticionIngresoPI(PagoOnLinePeticionIngresoPI value) {
        return new JAXBElement<PagoOnLinePeticionIngresoPI>(_EnvioPagoOnLinePeticionIngresoPI_QNAME, PagoOnLinePeticionIngresoPI.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionAnularPI }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionAnularPI }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionAnularPI")
    public JAXBElement<PagoOnLinePeticionAnularPI> createEnvioPagoOnLinePeticionAnularPI(PagoOnLinePeticionAnularPI value) {
        return new JAXBElement<PagoOnLinePeticionAnularPI>(_EnvioPagoOnLinePeticionAnularPI_QNAME, PagoOnLinePeticionAnularPI.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionConfirmarMX }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionConfirmarMX }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionConfirmarMX")
    public JAXBElement<PagoOnLinePeticionConfirmarMX> createEnvioPagoOnLinePeticionConfirmarMX(PagoOnLinePeticionConfirmarMX value) {
        return new JAXBElement<PagoOnLinePeticionConfirmarMX>(_EnvioPagoOnLinePeticionConfirmarMX_QNAME, PagoOnLinePeticionConfirmarMX.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoEstandar }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoEstandar }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionIngresoEstandar")
    public JAXBElement<PagoOnLinePeticionIngresoEstandar> createEnvioPagoOnLinePeticionIngresoEstandar(PagoOnLinePeticionIngresoEstandar value) {
        return new JAXBElement<PagoOnLinePeticionIngresoEstandar>(_EnvioPagoOnLinePeticionIngresoEstandar_QNAME, PagoOnLinePeticionIngresoEstandar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionAnularCrg }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionAnularCrg }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionAnularCrg")
    public JAXBElement<PagoOnLinePeticionAnularCrg> createEnvioPagoOnLinePeticionAnularCrg(PagoOnLinePeticionAnularCrg value) {
        return new JAXBElement<PagoOnLinePeticionAnularCrg>(_EnvioPagoOnLinePeticionAnularCrg_QNAME, PagoOnLinePeticionAnularCrg.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionConsultarCrg }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionConsultarCrg }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionConsultarCrg")
    public JAXBElement<PagoOnLinePeticionConsultarCrg> createEnvioPagoOnLinePeticionConsultarCrg(PagoOnLinePeticionConsultarCrg value) {
        return new JAXBElement<PagoOnLinePeticionConsultarCrg>(_EnvioPagoOnLinePeticionConsultarCrg_QNAME, PagoOnLinePeticionConsultarCrg.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoEstandarCrg }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoEstandarCrg }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionIngresoEstandarCrg")
    public JAXBElement<PagoOnLinePeticionIngresoEstandarCrg> createEnvioPagoOnLinePeticionIngresoEstandarCrg(PagoOnLinePeticionIngresoEstandarCrg value) {
        return new JAXBElement<PagoOnLinePeticionIngresoEstandarCrg>(_EnvioPagoOnLinePeticionIngresoEstandarCrg_QNAME, PagoOnLinePeticionIngresoEstandarCrg.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoMesaCrg }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoMesaCrg }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionIngresoMesaCrg")
    public JAXBElement<PagoOnLinePeticionIngresoMesaCrg> createEnvioPagoOnLinePeticionIngresoMesaCrg(PagoOnLinePeticionIngresoMesaCrg value) {
        return new JAXBElement<PagoOnLinePeticionIngresoMesaCrg>(_EnvioPagoOnLinePeticionIngresoMesaCrg_QNAME, PagoOnLinePeticionIngresoMesaCrg.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoEstandarPICrg }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoEstandarPICrg }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionIngresoEstandarPICrg")
    public JAXBElement<PagoOnLinePeticionIngresoEstandarPICrg> createEnvioPagoOnLinePeticionIngresoEstandarPICrg(PagoOnLinePeticionIngresoEstandarPICrg value) {
        return new JAXBElement<PagoOnLinePeticionIngresoEstandarPICrg>(_EnvioPagoOnLinePeticionIngresoEstandarPICrg_QNAME, PagoOnLinePeticionIngresoEstandarPICrg.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoMesaPICrg }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoMesaPICrg }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionIngresoMesaPICrg")
    public JAXBElement<PagoOnLinePeticionIngresoMesaPICrg> createEnvioPagoOnLinePeticionIngresoMesaPICrg(PagoOnLinePeticionIngresoMesaPICrg value) {
        return new JAXBElement<PagoOnLinePeticionIngresoMesaPICrg>(_EnvioPagoOnLinePeticionIngresoMesaPICrg_QNAME, PagoOnLinePeticionIngresoMesaPICrg.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoLegacy }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PagoOnLinePeticionIngresoLegacy }{@code >}
     */
    @XmlElementDecl(namespace = "http://ssf.ws/productos/servicios.pagos/integracionmesa", name = "envioPagoOnLinePeticionIngresoLegacy")
    public JAXBElement<PagoOnLinePeticionIngresoLegacy> createEnvioPagoOnLinePeticionIngresoLegacy(PagoOnLinePeticionIngresoLegacy value) {
        return new JAXBElement<PagoOnLinePeticionIngresoLegacy>(_EnvioPagoOnLinePeticionIngresoLegacy_QNAME, PagoOnLinePeticionIngresoLegacy.class, null, value);
    }

}
