
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PagoOnLinePeticionAnular complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PagoOnLinePeticionAnular"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Anulaciones" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}mesaDeDineroPagoOnLinePeticionAnularType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagoOnLinePeticionAnular", propOrder = {
    "anulaciones"
})
public class PagoOnLinePeticionAnular {

    @XmlElement(name = "Anulaciones", required = true, nillable = true)
    protected MesaDeDineroPagoOnLinePeticionAnularType anulaciones;

    /**
     * Obtiene el valor de la propiedad anulaciones.
     * 
     * @return
     *     possible object is
     *     {@link MesaDeDineroPagoOnLinePeticionAnularType }
     *     
     */
    public MesaDeDineroPagoOnLinePeticionAnularType getAnulaciones() {
        return anulaciones;
    }

    /**
     * Define el valor de la propiedad anulaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link MesaDeDineroPagoOnLinePeticionAnularType }
     *     
     */
    public void setAnulaciones(MesaDeDineroPagoOnLinePeticionAnularType value) {
        this.anulaciones = value;
    }

}
