
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para mesaDeDineroPagoOnLinePeticionIngresoEstandarType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="mesaDeDineroPagoOnLinePeticionIngresoEstandarType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CodigoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NumeroOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="TipoOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="rutClienteOrdenante" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}RutType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SucursalRutOrdenante" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="NombreClienteOrdenante_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteOrdenante_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteOrdenante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoDcvOrdenante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MontoOperacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="FechaValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="rutClienteBeneficiario" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}RutType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SucursalRutBeneficiario" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="CodigoDcvBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreClienteBeneficiario_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BancoBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemesa_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemesa_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemesa_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionRemesa_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="DetalleGastos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HoraValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CantidadContratos" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReferenciaContrato_10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FormaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorSesion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionReceptor_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionReceptor_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionReceptor_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionReceptor_4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionReceptor_5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InformacionReceptor_6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="DireccionBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ComunaCiudadBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mesaDeDineroPagoOnLinePeticionIngresoEstandarType", propOrder = {
    "codigoProducto",
    "numeroOperacion",
    "tipoOperacion",
    "indicadorAccion",
    "fechaOperacion",
    "moneda",
    "rutClienteOrdenante",
    "sucursalRutOrdenante",
    "nombreClienteOrdenante1",
    "nombreClienteOrdenante2",
    "cuentaCorrienteOrdenante",
    "codigoDcvOrdenante",
    "montoOperacion",
    "fechaValor",
    "rutClienteBeneficiario",
    "sucursalRutBeneficiario",
    "codigoDcvBeneficiario",
    "nombreClienteBeneficiario1",
    "nombreClienteBeneficiario2",
    "bancoBeneficiario",
    "cuentaCorrienteBeneficiario",
    "informacionRemesa1",
    "informacionRemesa2",
    "informacionRemesa3",
    "informacionRemesa4",
    "detalleGastos",
    "horaValor",
    "cantidadContratos",
    "referenciaContrato1",
    "referenciaContrato2",
    "referenciaContrato3",
    "referenciaContrato4",
    "referenciaContrato5",
    "referenciaContrato6",
    "referenciaContrato7",
    "referenciaContrato8",
    "referenciaContrato9",
    "referenciaContrato10",
    "formaPago",
    "indicadorSesion",
    "informacionReceptor1",
    "informacionReceptor2",
    "informacionReceptor3",
    "informacionReceptor4",
    "informacionReceptor5",
    "informacionReceptor6",
    "direccionBeneficiario",
    "comunaCiudadBeneficiario"
})
public class MesaDeDineroPagoOnLinePeticionIngresoEstandarType {

    @XmlElement(name = "CodigoProducto")
    protected String codigoProducto;
    @XmlElement(name = "NumeroOperacion")
    protected int numeroOperacion;
    @XmlElement(name = "TipoOperacion")
    protected String tipoOperacion;
    @XmlElement(name = "IndicadorAccion")
    protected String indicadorAccion;
    @XmlElement(name = "FechaOperacion")
    protected String fechaOperacion;
    @XmlElement(name = "Moneda")
    protected String moneda;
    protected RutType rutClienteOrdenante;
    @XmlElement(name = "SucursalRutOrdenante")
    @XmlSchemaType(name = "unsignedByte")
    protected short sucursalRutOrdenante;
    @XmlElement(name = "NombreClienteOrdenante_1")
    protected String nombreClienteOrdenante1;
    @XmlElement(name = "NombreClienteOrdenante_2")
    protected String nombreClienteOrdenante2;
    @XmlElement(name = "CuentaCorrienteOrdenante")
    protected String cuentaCorrienteOrdenante;
    @XmlElement(name = "CodigoDcvOrdenante")
    protected String codigoDcvOrdenante;
    @XmlElement(name = "MontoOperacion", required = true)
    protected BigDecimal montoOperacion;
    @XmlElement(name = "FechaValor")
    protected String fechaValor;
    protected RutType rutClienteBeneficiario;
    @XmlElement(name = "SucursalRutBeneficiario")
    @XmlSchemaType(name = "unsignedByte")
    protected short sucursalRutBeneficiario;
    @XmlElement(name = "CodigoDcvBeneficiario")
    protected String codigoDcvBeneficiario;
    @XmlElement(name = "NombreClienteBeneficiario_1")
    protected String nombreClienteBeneficiario1;
    @XmlElement(name = "NombreClienteBeneficiario_2")
    protected String nombreClienteBeneficiario2;
    @XmlElement(name = "BancoBeneficiario")
    protected String bancoBeneficiario;
    @XmlElement(name = "CuentaCorrienteBeneficiario")
    protected String cuentaCorrienteBeneficiario;
    @XmlElement(name = "InformacionRemesa_1")
    protected String informacionRemesa1;
    @XmlElement(name = "InformacionRemesa_2")
    protected String informacionRemesa2;
    @XmlElement(name = "InformacionRemesa_3")
    protected String informacionRemesa3;
    @XmlElement(name = "InformacionRemesa_4")
    protected String informacionRemesa4;
    @XmlElement(name = "DetalleGastos")
    protected String detalleGastos;
    @XmlElement(name = "HoraValor")
    protected String horaValor;
    @XmlElement(name = "CantidadContratos")
    @XmlSchemaType(name = "unsignedByte")
    protected short cantidadContratos;
    @XmlElement(name = "ReferenciaContrato_1")
    protected String referenciaContrato1;
    @XmlElement(name = "ReferenciaContrato_2")
    protected String referenciaContrato2;
    @XmlElement(name = "ReferenciaContrato_3")
    protected String referenciaContrato3;
    @XmlElement(name = "ReferenciaContrato_4")
    protected String referenciaContrato4;
    @XmlElement(name = "ReferenciaContrato_5")
    protected String referenciaContrato5;
    @XmlElement(name = "ReferenciaContrato_6")
    protected String referenciaContrato6;
    @XmlElement(name = "ReferenciaContrato_7")
    protected String referenciaContrato7;
    @XmlElement(name = "ReferenciaContrato_8")
    protected String referenciaContrato8;
    @XmlElement(name = "ReferenciaContrato_9")
    protected String referenciaContrato9;
    @XmlElement(name = "ReferenciaContrato_10")
    protected String referenciaContrato10;
    @XmlElement(name = "FormaPago")
    protected String formaPago;
    @XmlElement(name = "IndicadorSesion")
    protected String indicadorSesion;
    @XmlElement(name = "InformacionReceptor_1")
    protected String informacionReceptor1;
    @XmlElement(name = "InformacionReceptor_2")
    protected String informacionReceptor2;
    @XmlElement(name = "InformacionReceptor_3")
    protected String informacionReceptor3;
    @XmlElement(name = "InformacionReceptor_4")
    protected String informacionReceptor4;
    @XmlElement(name = "InformacionReceptor_5")
    protected String informacionReceptor5;
    @XmlElement(name = "InformacionReceptor_6")
    protected String informacionReceptor6;
    @XmlElement(name = "DireccionBeneficiario")
    protected String direccionBeneficiario;
    @XmlElement(name = "ComunaCiudadBeneficiario")
    protected String comunaCiudadBeneficiario;

    /**
     * Obtiene el valor de la propiedad codigoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Define el valor de la propiedad codigoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProducto(String value) {
        this.codigoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroOperacion.
     * 
     */
    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    /**
     * Define el valor de la propiedad numeroOperacion.
     * 
     */
    public void setNumeroOperacion(int value) {
        this.numeroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * Define el valor de la propiedad tipoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperacion(String value) {
        this.tipoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorAccion() {
        return indicadorAccion;
    }

    /**
     * Define el valor de la propiedad indicadorAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorAccion(String value) {
        this.indicadorAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Define el valor de la propiedad fechaOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaOperacion(String value) {
        this.fechaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad rutClienteOrdenante.
     * 
     * @return
     *     possible object is
     *     {@link RutType }
     *     
     */
    public RutType getRutClienteOrdenante() {
        return rutClienteOrdenante;
    }

    /**
     * Define el valor de la propiedad rutClienteOrdenante.
     * 
     * @param value
     *     allowed object is
     *     {@link RutType }
     *     
     */
    public void setRutClienteOrdenante(RutType value) {
        this.rutClienteOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursalRutOrdenante.
     * 
     */
    public short getSucursalRutOrdenante() {
        return sucursalRutOrdenante;
    }

    /**
     * Define el valor de la propiedad sucursalRutOrdenante.
     * 
     */
    public void setSucursalRutOrdenante(short value) {
        this.sucursalRutOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteOrdenante1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteOrdenante1() {
        return nombreClienteOrdenante1;
    }

    /**
     * Define el valor de la propiedad nombreClienteOrdenante1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteOrdenante1(String value) {
        this.nombreClienteOrdenante1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteOrdenante2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteOrdenante2() {
        return nombreClienteOrdenante2;
    }

    /**
     * Define el valor de la propiedad nombreClienteOrdenante2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteOrdenante2(String value) {
        this.nombreClienteOrdenante2 = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteOrdenante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteOrdenante() {
        return cuentaCorrienteOrdenante;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteOrdenante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteOrdenante(String value) {
        this.cuentaCorrienteOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoDcvOrdenante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDcvOrdenante() {
        return codigoDcvOrdenante;
    }

    /**
     * Define el valor de la propiedad codigoDcvOrdenante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDcvOrdenante(String value) {
        this.codigoDcvOrdenante = value;
    }

    /**
     * Obtiene el valor de la propiedad montoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoOperacion() {
        return montoOperacion;
    }

    /**
     * Define el valor de la propiedad montoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoOperacion(BigDecimal value) {
        this.montoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaValor() {
        return fechaValor;
    }

    /**
     * Define el valor de la propiedad fechaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaValor(String value) {
        this.fechaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad rutClienteBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link RutType }
     *     
     */
    public RutType getRutClienteBeneficiario() {
        return rutClienteBeneficiario;
    }

    /**
     * Define el valor de la propiedad rutClienteBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link RutType }
     *     
     */
    public void setRutClienteBeneficiario(RutType value) {
        this.rutClienteBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursalRutBeneficiario.
     * 
     */
    public short getSucursalRutBeneficiario() {
        return sucursalRutBeneficiario;
    }

    /**
     * Define el valor de la propiedad sucursalRutBeneficiario.
     * 
     */
    public void setSucursalRutBeneficiario(short value) {
        this.sucursalRutBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoDcvBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDcvBeneficiario() {
        return codigoDcvBeneficiario;
    }

    /**
     * Define el valor de la propiedad codigoDcvBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDcvBeneficiario(String value) {
        this.codigoDcvBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario1() {
        return nombreClienteBeneficiario1;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario1(String value) {
        this.nombreClienteBeneficiario1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreClienteBeneficiario2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreClienteBeneficiario2() {
        return nombreClienteBeneficiario2;
    }

    /**
     * Define el valor de la propiedad nombreClienteBeneficiario2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreClienteBeneficiario2(String value) {
        this.nombreClienteBeneficiario2 = value;
    }

    /**
     * Obtiene el valor de la propiedad bancoBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBancoBeneficiario() {
        return bancoBeneficiario;
    }

    /**
     * Define el valor de la propiedad bancoBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBancoBeneficiario(String value) {
        this.bancoBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteBeneficiario() {
        return cuentaCorrienteBeneficiario;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteBeneficiario(String value) {
        this.cuentaCorrienteBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemesa1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemesa1() {
        return informacionRemesa1;
    }

    /**
     * Define el valor de la propiedad informacionRemesa1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemesa1(String value) {
        this.informacionRemesa1 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemesa2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemesa2() {
        return informacionRemesa2;
    }

    /**
     * Define el valor de la propiedad informacionRemesa2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemesa2(String value) {
        this.informacionRemesa2 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemesa3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemesa3() {
        return informacionRemesa3;
    }

    /**
     * Define el valor de la propiedad informacionRemesa3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemesa3(String value) {
        this.informacionRemesa3 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionRemesa4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionRemesa4() {
        return informacionRemesa4;
    }

    /**
     * Define el valor de la propiedad informacionRemesa4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionRemesa4(String value) {
        this.informacionRemesa4 = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleGastos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetalleGastos() {
        return detalleGastos;
    }

    /**
     * Define el valor de la propiedad detalleGastos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetalleGastos(String value) {
        this.detalleGastos = value;
    }

    /**
     * Obtiene el valor de la propiedad horaValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraValor() {
        return horaValor;
    }

    /**
     * Define el valor de la propiedad horaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraValor(String value) {
        this.horaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidadContratos.
     * 
     */
    public short getCantidadContratos() {
        return cantidadContratos;
    }

    /**
     * Define el valor de la propiedad cantidadContratos.
     * 
     */
    public void setCantidadContratos(short value) {
        this.cantidadContratos = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato1() {
        return referenciaContrato1;
    }

    /**
     * Define el valor de la propiedad referenciaContrato1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato1(String value) {
        this.referenciaContrato1 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato2() {
        return referenciaContrato2;
    }

    /**
     * Define el valor de la propiedad referenciaContrato2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato2(String value) {
        this.referenciaContrato2 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato3() {
        return referenciaContrato3;
    }

    /**
     * Define el valor de la propiedad referenciaContrato3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato3(String value) {
        this.referenciaContrato3 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato4() {
        return referenciaContrato4;
    }

    /**
     * Define el valor de la propiedad referenciaContrato4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato4(String value) {
        this.referenciaContrato4 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato5() {
        return referenciaContrato5;
    }

    /**
     * Define el valor de la propiedad referenciaContrato5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato5(String value) {
        this.referenciaContrato5 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato6() {
        return referenciaContrato6;
    }

    /**
     * Define el valor de la propiedad referenciaContrato6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato6(String value) {
        this.referenciaContrato6 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato7.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato7() {
        return referenciaContrato7;
    }

    /**
     * Define el valor de la propiedad referenciaContrato7.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato7(String value) {
        this.referenciaContrato7 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato8.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato8() {
        return referenciaContrato8;
    }

    /**
     * Define el valor de la propiedad referenciaContrato8.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato8(String value) {
        this.referenciaContrato8 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato9.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato9() {
        return referenciaContrato9;
    }

    /**
     * Define el valor de la propiedad referenciaContrato9.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato9(String value) {
        this.referenciaContrato9 = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaContrato10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaContrato10() {
        return referenciaContrato10;
    }

    /**
     * Define el valor de la propiedad referenciaContrato10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaContrato10(String value) {
        this.referenciaContrato10 = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * Define el valor de la propiedad formaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPago(String value) {
        this.formaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorSesion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorSesion() {
        return indicadorSesion;
    }

    /**
     * Define el valor de la propiedad indicadorSesion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorSesion(String value) {
        this.indicadorSesion = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionReceptor1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionReceptor1() {
        return informacionReceptor1;
    }

    /**
     * Define el valor de la propiedad informacionReceptor1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionReceptor1(String value) {
        this.informacionReceptor1 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionReceptor2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionReceptor2() {
        return informacionReceptor2;
    }

    /**
     * Define el valor de la propiedad informacionReceptor2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionReceptor2(String value) {
        this.informacionReceptor2 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionReceptor3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionReceptor3() {
        return informacionReceptor3;
    }

    /**
     * Define el valor de la propiedad informacionReceptor3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionReceptor3(String value) {
        this.informacionReceptor3 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionReceptor4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionReceptor4() {
        return informacionReceptor4;
    }

    /**
     * Define el valor de la propiedad informacionReceptor4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionReceptor4(String value) {
        this.informacionReceptor4 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionReceptor5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionReceptor5() {
        return informacionReceptor5;
    }

    /**
     * Define el valor de la propiedad informacionReceptor5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionReceptor5(String value) {
        this.informacionReceptor5 = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionReceptor6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionReceptor6() {
        return informacionReceptor6;
    }

    /**
     * Define el valor de la propiedad informacionReceptor6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionReceptor6(String value) {
        this.informacionReceptor6 = value;
    }

    /**
     * Obtiene el valor de la propiedad direccionBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionBeneficiario() {
        return direccionBeneficiario;
    }

    /**
     * Define el valor de la propiedad direccionBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionBeneficiario(String value) {
        this.direccionBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad comunaCiudadBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComunaCiudadBeneficiario() {
        return comunaCiudadBeneficiario;
    }

    /**
     * Define el valor de la propiedad comunaCiudadBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComunaCiudadBeneficiario(String value) {
        this.comunaCiudadBeneficiario = value;
    }

}
