
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para mesaDeDineroPagoOnLinePeticionIngresoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="mesaDeDineroPagoOnLinePeticionIngresoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="rutCliente" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}RutType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TipoOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Ejecutivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FormaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoValuta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NombreCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Banco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteBeneficiarioVendedor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ClaveAbif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoDcvComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CuentaCorrienteVendedor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoDcvVendedor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TipoReajustabilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Custodia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CodigoMndaFx" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FechaValorMx" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FormaPagoNegocio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="IndicadorSesion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NumeroOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="SucursalRut" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&amp;gt;
 *         &amp;lt;element name="MontoOperacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MontoOriginal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="TasaInteres" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MontoInteres" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MontoVencimiento" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="TasaPacto" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MontoFinal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MontoNominal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="TasaDescuento" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="ValorTasaDescuento" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="NumeroInstrumentos" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MontoTotalTransado" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="MontoFX" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="TasaCambio" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mesaDeDineroPagoOnLinePeticionIngresoType", propOrder = {
    "rutCliente",
    "codigoProducto",
    "tipoOperacion",
    "indicadorAccion",
    "fechaOperacion",
    "ejecutivo",
    "moneda",
    "formaPago",
    "codigoValuta",
    "nombreCliente",
    "banco",
    "cuentaCorrienteBeneficiarioVendedor",
    "claveAbif",
    "cuentaCorrienteComprador",
    "codigoDcvComprador",
    "cuentaCorrienteVendedor",
    "codigoDcvVendedor",
    "fechaInicio",
    "fechaVencimiento",
    "tipoReajustabilidad",
    "custodia",
    "codigoMndaFx",
    "fechaValorMx",
    "formaPagoNegocio",
    "indicadorSesion",
    "numeroOperacion",
    "sucursalRut",
    "montoOperacion",
    "montoOriginal",
    "tasaInteres",
    "montoInteres",
    "montoVencimiento",
    "tasaPacto",
    "montoFinal",
    "montoNominal",
    "tasaDescuento",
    "valorTasaDescuento",
    "numeroInstrumentos",
    "montoTotalTransado",
    "montoFX",
    "tasaCambio"
})
public class MesaDeDineroPagoOnLinePeticionIngresoType {

    protected RutType rutCliente;
    @XmlElement(name = "CodigoProducto")
    protected String codigoProducto;
    @XmlElement(name = "TipoOperacion")
    protected String tipoOperacion;
    @XmlElement(name = "IndicadorAccion")
    protected String indicadorAccion;
    @XmlElement(name = "FechaOperacion")
    protected String fechaOperacion;
    @XmlElement(name = "Ejecutivo")
    protected String ejecutivo;
    @XmlElement(name = "Moneda")
    protected String moneda;
    @XmlElement(name = "FormaPago")
    protected String formaPago;
    @XmlElement(name = "CodigoValuta")
    protected String codigoValuta;
    @XmlElement(name = "NombreCliente")
    protected String nombreCliente;
    @XmlElement(name = "Banco")
    protected String banco;
    @XmlElement(name = "CuentaCorrienteBeneficiarioVendedor")
    protected String cuentaCorrienteBeneficiarioVendedor;
    @XmlElement(name = "ClaveAbif")
    protected String claveAbif;
    @XmlElement(name = "CuentaCorrienteComprador")
    protected String cuentaCorrienteComprador;
    @XmlElement(name = "CodigoDcvComprador")
    protected String codigoDcvComprador;
    @XmlElement(name = "CuentaCorrienteVendedor")
    protected String cuentaCorrienteVendedor;
    @XmlElement(name = "CodigoDcvVendedor")
    protected String codigoDcvVendedor;
    @XmlElement(name = "FechaInicio")
    protected String fechaInicio;
    @XmlElement(name = "FechaVencimiento")
    protected String fechaVencimiento;
    @XmlElement(name = "TipoReajustabilidad")
    protected String tipoReajustabilidad;
    @XmlElement(name = "Custodia")
    protected String custodia;
    @XmlElement(name = "CodigoMndaFx")
    protected String codigoMndaFx;
    @XmlElement(name = "FechaValorMx")
    protected String fechaValorMx;
    @XmlElement(name = "FormaPagoNegocio")
    protected String formaPagoNegocio;
    @XmlElement(name = "IndicadorSesion")
    protected String indicadorSesion;
    @XmlElement(name = "NumeroOperacion")
    protected int numeroOperacion;
    @XmlElement(name = "SucursalRut")
    @XmlSchemaType(name = "unsignedByte")
    protected short sucursalRut;
    @XmlElement(name = "MontoOperacion", required = true)
    protected BigDecimal montoOperacion;
    @XmlElement(name = "MontoOriginal", required = true)
    protected BigDecimal montoOriginal;
    @XmlElement(name = "TasaInteres", required = true)
    protected BigDecimal tasaInteres;
    @XmlElement(name = "MontoInteres", required = true)
    protected BigDecimal montoInteres;
    @XmlElement(name = "MontoVencimiento", required = true)
    protected BigDecimal montoVencimiento;
    @XmlElement(name = "TasaPacto", required = true)
    protected BigDecimal tasaPacto;
    @XmlElement(name = "MontoFinal", required = true)
    protected BigDecimal montoFinal;
    @XmlElement(name = "MontoNominal", required = true)
    protected BigDecimal montoNominal;
    @XmlElement(name = "TasaDescuento", required = true)
    protected BigDecimal tasaDescuento;
    @XmlElement(name = "ValorTasaDescuento", required = true)
    protected BigDecimal valorTasaDescuento;
    @XmlElement(name = "NumeroInstrumentos", required = true)
    protected BigDecimal numeroInstrumentos;
    @XmlElement(name = "MontoTotalTransado", required = true)
    protected BigDecimal montoTotalTransado;
    @XmlElement(name = "MontoFX", required = true)
    protected BigDecimal montoFX;
    @XmlElement(name = "TasaCambio", required = true)
    protected BigDecimal tasaCambio;

    /**
     * Obtiene el valor de la propiedad rutCliente.
     * 
     * @return
     *     possible object is
     *     {@link RutType }
     *     
     */
    public RutType getRutCliente() {
        return rutCliente;
    }

    /**
     * Define el valor de la propiedad rutCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link RutType }
     *     
     */
    public void setRutCliente(RutType value) {
        this.rutCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Define el valor de la propiedad codigoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProducto(String value) {
        this.codigoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * Define el valor de la propiedad tipoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperacion(String value) {
        this.tipoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorAccion() {
        return indicadorAccion;
    }

    /**
     * Define el valor de la propiedad indicadorAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorAccion(String value) {
        this.indicadorAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Define el valor de la propiedad fechaOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaOperacion(String value) {
        this.fechaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad ejecutivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEjecutivo() {
        return ejecutivo;
    }

    /**
     * Define el valor de la propiedad ejecutivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEjecutivo(String value) {
        this.ejecutivo = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * Define el valor de la propiedad formaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPago(String value) {
        this.formaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoValuta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoValuta() {
        return codigoValuta;
    }

    /**
     * Define el valor de la propiedad codigoValuta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoValuta(String value) {
        this.codigoValuta = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Define el valor de la propiedad nombreCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCliente(String value) {
        this.nombreCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad banco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Define el valor de la propiedad banco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanco(String value) {
        this.banco = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteBeneficiarioVendedor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteBeneficiarioVendedor() {
        return cuentaCorrienteBeneficiarioVendedor;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteBeneficiarioVendedor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteBeneficiarioVendedor(String value) {
        this.cuentaCorrienteBeneficiarioVendedor = value;
    }

    /**
     * Obtiene el valor de la propiedad claveAbif.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveAbif() {
        return claveAbif;
    }

    /**
     * Define el valor de la propiedad claveAbif.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveAbif(String value) {
        this.claveAbif = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteComprador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteComprador() {
        return cuentaCorrienteComprador;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteComprador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteComprador(String value) {
        this.cuentaCorrienteComprador = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoDcvComprador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDcvComprador() {
        return codigoDcvComprador;
    }

    /**
     * Define el valor de la propiedad codigoDcvComprador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDcvComprador(String value) {
        this.codigoDcvComprador = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCorrienteVendedor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCorrienteVendedor() {
        return cuentaCorrienteVendedor;
    }

    /**
     * Define el valor de la propiedad cuentaCorrienteVendedor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCorrienteVendedor(String value) {
        this.cuentaCorrienteVendedor = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoDcvVendedor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDcvVendedor() {
        return codigoDcvVendedor;
    }

    /**
     * Define el valor de la propiedad codigoDcvVendedor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDcvVendedor(String value) {
        this.codigoDcvVendedor = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Define el valor de la propiedad fechaInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInicio(String value) {
        this.fechaInicio = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Define el valor de la propiedad fechaVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVencimiento(String value) {
        this.fechaVencimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoReajustabilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoReajustabilidad() {
        return tipoReajustabilidad;
    }

    /**
     * Define el valor de la propiedad tipoReajustabilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoReajustabilidad(String value) {
        this.tipoReajustabilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad custodia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustodia() {
        return custodia;
    }

    /**
     * Define el valor de la propiedad custodia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustodia(String value) {
        this.custodia = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoMndaFx.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoMndaFx() {
        return codigoMndaFx;
    }

    /**
     * Define el valor de la propiedad codigoMndaFx.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoMndaFx(String value) {
        this.codigoMndaFx = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaValorMx.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaValorMx() {
        return fechaValorMx;
    }

    /**
     * Define el valor de la propiedad fechaValorMx.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaValorMx(String value) {
        this.fechaValorMx = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPagoNegocio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPagoNegocio() {
        return formaPagoNegocio;
    }

    /**
     * Define el valor de la propiedad formaPagoNegocio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPagoNegocio(String value) {
        this.formaPagoNegocio = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorSesion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorSesion() {
        return indicadorSesion;
    }

    /**
     * Define el valor de la propiedad indicadorSesion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorSesion(String value) {
        this.indicadorSesion = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroOperacion.
     * 
     */
    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    /**
     * Define el valor de la propiedad numeroOperacion.
     * 
     */
    public void setNumeroOperacion(int value) {
        this.numeroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursalRut.
     * 
     */
    public short getSucursalRut() {
        return sucursalRut;
    }

    /**
     * Define el valor de la propiedad sucursalRut.
     * 
     */
    public void setSucursalRut(short value) {
        this.sucursalRut = value;
    }

    /**
     * Obtiene el valor de la propiedad montoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoOperacion() {
        return montoOperacion;
    }

    /**
     * Define el valor de la propiedad montoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoOperacion(BigDecimal value) {
        this.montoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad montoOriginal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoOriginal() {
        return montoOriginal;
    }

    /**
     * Define el valor de la propiedad montoOriginal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoOriginal(BigDecimal value) {
        this.montoOriginal = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaInteres.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTasaInteres() {
        return tasaInteres;
    }

    /**
     * Define el valor de la propiedad tasaInteres.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTasaInteres(BigDecimal value) {
        this.tasaInteres = value;
    }

    /**
     * Obtiene el valor de la propiedad montoInteres.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoInteres() {
        return montoInteres;
    }

    /**
     * Define el valor de la propiedad montoInteres.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoInteres(BigDecimal value) {
        this.montoInteres = value;
    }

    /**
     * Obtiene el valor de la propiedad montoVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoVencimiento() {
        return montoVencimiento;
    }

    /**
     * Define el valor de la propiedad montoVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoVencimiento(BigDecimal value) {
        this.montoVencimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaPacto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTasaPacto() {
        return tasaPacto;
    }

    /**
     * Define el valor de la propiedad tasaPacto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTasaPacto(BigDecimal value) {
        this.tasaPacto = value;
    }

    /**
     * Obtiene el valor de la propiedad montoFinal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoFinal() {
        return montoFinal;
    }

    /**
     * Define el valor de la propiedad montoFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoFinal(BigDecimal value) {
        this.montoFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad montoNominal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoNominal() {
        return montoNominal;
    }

    /**
     * Define el valor de la propiedad montoNominal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoNominal(BigDecimal value) {
        this.montoNominal = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaDescuento.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTasaDescuento() {
        return tasaDescuento;
    }

    /**
     * Define el valor de la propiedad tasaDescuento.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTasaDescuento(BigDecimal value) {
        this.tasaDescuento = value;
    }

    /**
     * Obtiene el valor de la propiedad valorTasaDescuento.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorTasaDescuento() {
        return valorTasaDescuento;
    }

    /**
     * Define el valor de la propiedad valorTasaDescuento.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorTasaDescuento(BigDecimal value) {
        this.valorTasaDescuento = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroInstrumentos.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumeroInstrumentos() {
        return numeroInstrumentos;
    }

    /**
     * Define el valor de la propiedad numeroInstrumentos.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumeroInstrumentos(BigDecimal value) {
        this.numeroInstrumentos = value;
    }

    /**
     * Obtiene el valor de la propiedad montoTotalTransado.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoTotalTransado() {
        return montoTotalTransado;
    }

    /**
     * Define el valor de la propiedad montoTotalTransado.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoTotalTransado(BigDecimal value) {
        this.montoTotalTransado = value;
    }

    /**
     * Obtiene el valor de la propiedad montoFX.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoFX() {
        return montoFX;
    }

    /**
     * Define el valor de la propiedad montoFX.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoFX(BigDecimal value) {
        this.montoFX = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaCambio.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTasaCambio() {
        return tasaCambio;
    }

    /**
     * Define el valor de la propiedad tasaCambio.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTasaCambio(BigDecimal value) {
        this.tasaCambio = value;
    }

}
