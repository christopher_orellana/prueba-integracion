
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PagoOnLinePeticionConfirmarMX complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PagoOnLinePeticionConfirmarMX"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Confirmaciones" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}mesaDeDineroPagoOnLinePeticionConfirmarMXType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagoOnLinePeticionConfirmarMX", propOrder = {
    "confirmaciones"
})
public class PagoOnLinePeticionConfirmarMX {

    @XmlElement(name = "Confirmaciones", required = true, nillable = true)
    protected MesaDeDineroPagoOnLinePeticionConfirmarMXType confirmaciones;

    /**
     * Obtiene el valor de la propiedad confirmaciones.
     * 
     * @return
     *     possible object is
     *     {@link MesaDeDineroPagoOnLinePeticionConfirmarMXType }
     *     
     */
    public MesaDeDineroPagoOnLinePeticionConfirmarMXType getConfirmaciones() {
        return confirmaciones;
    }

    /**
     * Define el valor de la propiedad confirmaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link MesaDeDineroPagoOnLinePeticionConfirmarMXType }
     *     
     */
    public void setConfirmaciones(MesaDeDineroPagoOnLinePeticionConfirmarMXType value) {
        this.confirmaciones = value;
    }

}
