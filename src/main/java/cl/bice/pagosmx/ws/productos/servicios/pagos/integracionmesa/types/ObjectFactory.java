
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionIngreso }
     * 
     */
    public PagoOnLinePeticionIngreso createPagoOnLinePeticionIngreso() {
        return new PagoOnLinePeticionIngreso();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionIngresoType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionIngresoType createMesaDeDineroPagoOnLinePeticionIngresoType() {
        return new MesaDeDineroPagoOnLinePeticionIngresoType();
    }

    /**
     * Create an instance of {@link RutType }
     * 
     */
    public RutType createRutType() {
        return new RutType();
    }

    /**
     * Create an instance of {@link PagoOnLineRespuesta }
     * 
     */
    public PagoOnLineRespuesta createPagoOnLineRespuesta() {
        return new PagoOnLineRespuesta();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLineRespuestaType }
     * 
     */
    public MesaDeDineroPagoOnLineRespuestaType createMesaDeDineroPagoOnLineRespuestaType() {
        return new MesaDeDineroPagoOnLineRespuestaType();
    }

    /**
     * Create an instance of {@link DetalleMensajesOperacionType }
     * 
     */
    public DetalleMensajesOperacionType createDetalleMensajesOperacionType() {
        return new DetalleMensajesOperacionType();
    }

    /**
     * Create an instance of {@link PagoDocumentoOnLinePeticionIngreso }
     * 
     */
    public PagoDocumentoOnLinePeticionIngreso createPagoDocumentoOnLinePeticionIngreso() {
        return new PagoDocumentoOnLinePeticionIngreso();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoDocumentoOnLinePeticionIngresoType }
     * 
     */
    public MesaDeDineroPagoDocumentoOnLinePeticionIngresoType createMesaDeDineroPagoDocumentoOnLinePeticionIngresoType() {
        return new MesaDeDineroPagoDocumentoOnLinePeticionIngresoType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionAnular }
     * 
     */
    public PagoOnLinePeticionAnular createPagoOnLinePeticionAnular() {
        return new PagoOnLinePeticionAnular();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionAnularType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionAnularType createMesaDeDineroPagoOnLinePeticionAnularType() {
        return new MesaDeDineroPagoOnLinePeticionAnularType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionConsultar }
     * 
     */
    public PagoOnLinePeticionConsultar createPagoOnLinePeticionConsultar() {
        return new PagoOnLinePeticionConsultar();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionConsultarType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionConsultarType createMesaDeDineroPagoOnLinePeticionConsultarType() {
        return new MesaDeDineroPagoOnLinePeticionConsultarType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionIngresoPI }
     * 
     */
    public PagoOnLinePeticionIngresoPI createPagoOnLinePeticionIngresoPI() {
        return new PagoOnLinePeticionIngresoPI();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionIngresoPIType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionIngresoPIType createMesaDeDineroPagoOnLinePeticionIngresoPIType() {
        return new MesaDeDineroPagoOnLinePeticionIngresoPIType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionAnularPI }
     * 
     */
    public PagoOnLinePeticionAnularPI createPagoOnLinePeticionAnularPI() {
        return new PagoOnLinePeticionAnularPI();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionAnularPIType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionAnularPIType createMesaDeDineroPagoOnLinePeticionAnularPIType() {
        return new MesaDeDineroPagoOnLinePeticionAnularPIType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionConfirmarMX }
     * 
     */
    public PagoOnLinePeticionConfirmarMX createPagoOnLinePeticionConfirmarMX() {
        return new PagoOnLinePeticionConfirmarMX();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionConfirmarMXType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionConfirmarMXType createMesaDeDineroPagoOnLinePeticionConfirmarMXType() {
        return new MesaDeDineroPagoOnLinePeticionConfirmarMXType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionIngresoEstandar }
     * 
     */
    public PagoOnLinePeticionIngresoEstandar createPagoOnLinePeticionIngresoEstandar() {
        return new PagoOnLinePeticionIngresoEstandar();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionIngresoEstandarType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionIngresoEstandarType createMesaDeDineroPagoOnLinePeticionIngresoEstandarType() {
        return new MesaDeDineroPagoOnLinePeticionIngresoEstandarType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionAnularCrg }
     * 
     */
    public PagoOnLinePeticionAnularCrg createPagoOnLinePeticionAnularCrg() {
        return new PagoOnLinePeticionAnularCrg();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionAnularCrgType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionAnularCrgType createMesaDeDineroPagoOnLinePeticionAnularCrgType() {
        return new MesaDeDineroPagoOnLinePeticionAnularCrgType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionConsultarCrg }
     * 
     */
    public PagoOnLinePeticionConsultarCrg createPagoOnLinePeticionConsultarCrg() {
        return new PagoOnLinePeticionConsultarCrg();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionConsultarCrgType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionConsultarCrgType createMesaDeDineroPagoOnLinePeticionConsultarCrgType() {
        return new MesaDeDineroPagoOnLinePeticionConsultarCrgType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionIngresoEstandarCrg }
     * 
     */
    public PagoOnLinePeticionIngresoEstandarCrg createPagoOnLinePeticionIngresoEstandarCrg() {
        return new PagoOnLinePeticionIngresoEstandarCrg();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType createMesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType() {
        return new MesaDeDineroPagoOnLinePeticionIngresoEstandarCrgType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionIngresoMesaCrg }
     * 
     */
    public PagoOnLinePeticionIngresoMesaCrg createPagoOnLinePeticionIngresoMesaCrg() {
        return new PagoOnLinePeticionIngresoMesaCrg();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionIngresoMesaCrgType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionIngresoMesaCrgType createMesaDeDineroPagoOnLinePeticionIngresoMesaCrgType() {
        return new MesaDeDineroPagoOnLinePeticionIngresoMesaCrgType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionIngresoEstandarPICrg }
     * 
     */
    public PagoOnLinePeticionIngresoEstandarPICrg createPagoOnLinePeticionIngresoEstandarPICrg() {
        return new PagoOnLinePeticionIngresoEstandarPICrg();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionIngresoEstandarPICrgType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionIngresoEstandarPICrgType createMesaDeDineroPagoOnLinePeticionIngresoEstandarPICrgType() {
        return new MesaDeDineroPagoOnLinePeticionIngresoEstandarPICrgType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionIngresoMesaPICrg }
     * 
     */
    public PagoOnLinePeticionIngresoMesaPICrg createPagoOnLinePeticionIngresoMesaPICrg() {
        return new PagoOnLinePeticionIngresoMesaPICrg();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionIngresoMesaPICrgType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionIngresoMesaPICrgType createMesaDeDineroPagoOnLinePeticionIngresoMesaPICrgType() {
        return new MesaDeDineroPagoOnLinePeticionIngresoMesaPICrgType();
    }

    /**
     * Create an instance of {@link PagoOnLinePeticionIngresoLegacy }
     * 
     */
    public PagoOnLinePeticionIngresoLegacy createPagoOnLinePeticionIngresoLegacy() {
        return new PagoOnLinePeticionIngresoLegacy();
    }

    /**
     * Create an instance of {@link MesaDeDineroPagoOnLinePeticionIngresoLegacyType }
     * 
     */
    public MesaDeDineroPagoOnLinePeticionIngresoLegacyType createMesaDeDineroPagoOnLinePeticionIngresoLegacyType() {
        return new MesaDeDineroPagoOnLinePeticionIngresoLegacyType();
    }

}
