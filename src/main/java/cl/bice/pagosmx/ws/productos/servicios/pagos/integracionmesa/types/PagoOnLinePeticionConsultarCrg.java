
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PagoOnLinePeticionConsultarCrg complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PagoOnLinePeticionConsultarCrg"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Consultas" type="{http://ssf.ws/productos/servicios.pagos/integracionmesa/types}mesaDeDineroPagoOnLinePeticionConsultarCrgType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagoOnLinePeticionConsultarCrg", propOrder = {
    "consultas"
})
public class PagoOnLinePeticionConsultarCrg {

    @XmlElement(name = "Consultas", required = true, nillable = true)
    protected MesaDeDineroPagoOnLinePeticionConsultarCrgType consultas;

    /**
     * Obtiene el valor de la propiedad consultas.
     * 
     * @return
     *     possible object is
     *     {@link MesaDeDineroPagoOnLinePeticionConsultarCrgType }
     *     
     */
    public MesaDeDineroPagoOnLinePeticionConsultarCrgType getConsultas() {
        return consultas;
    }

    /**
     * Define el valor de la propiedad consultas.
     * 
     * @param value
     *     allowed object is
     *     {@link MesaDeDineroPagoOnLinePeticionConsultarCrgType }
     *     
     */
    public void setConsultas(MesaDeDineroPagoOnLinePeticionConsultarCrgType value) {
        this.consultas = value;
    }

}
