
package cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para mesaDeDineroPagoDocumentoOnLinePeticionIngresoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="mesaDeDineroPagoDocumentoOnLinePeticionIngresoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CodigoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="NumeroOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="NemonicoTipoInstrumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ValorNominal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="Tir" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="ValorCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *         &amp;lt;element name="ClaveOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CorrelativoRegistro" type="{http://www.w3.org/2001/XMLSchema}decimal"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mesaDeDineroPagoDocumentoOnLinePeticionIngresoType", propOrder = {
    "codigoProducto",
    "numeroOperacion",
    "nemonicoTipoInstrumento",
    "valorNominal",
    "tir",
    "valorCompra",
    "claveOperacion",
    "correlativoRegistro"
})
public class MesaDeDineroPagoDocumentoOnLinePeticionIngresoType {

    @XmlElement(name = "CodigoProducto")
    protected String codigoProducto;
    @XmlElement(name = "NumeroOperacion")
    protected int numeroOperacion;
    @XmlElement(name = "NemonicoTipoInstrumento")
    protected String nemonicoTipoInstrumento;
    @XmlElement(name = "ValorNominal", required = true)
    protected BigDecimal valorNominal;
    @XmlElement(name = "Tir", required = true)
    protected BigDecimal tir;
    @XmlElement(name = "ValorCompra", required = true)
    protected BigDecimal valorCompra;
    @XmlElement(name = "ClaveOperacion")
    protected String claveOperacion;
    @XmlElement(name = "CorrelativoRegistro", required = true)
    protected BigDecimal correlativoRegistro;

    /**
     * Obtiene el valor de la propiedad codigoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Define el valor de la propiedad codigoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProducto(String value) {
        this.codigoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroOperacion.
     * 
     */
    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    /**
     * Define el valor de la propiedad numeroOperacion.
     * 
     */
    public void setNumeroOperacion(int value) {
        this.numeroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nemonicoTipoInstrumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNemonicoTipoInstrumento() {
        return nemonicoTipoInstrumento;
    }

    /**
     * Define el valor de la propiedad nemonicoTipoInstrumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNemonicoTipoInstrumento(String value) {
        this.nemonicoTipoInstrumento = value;
    }

    /**
     * Obtiene el valor de la propiedad valorNominal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorNominal() {
        return valorNominal;
    }

    /**
     * Define el valor de la propiedad valorNominal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorNominal(BigDecimal value) {
        this.valorNominal = value;
    }

    /**
     * Obtiene el valor de la propiedad tir.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTir() {
        return tir;
    }

    /**
     * Define el valor de la propiedad tir.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTir(BigDecimal value) {
        this.tir = value;
    }

    /**
     * Obtiene el valor de la propiedad valorCompra.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorCompra() {
        return valorCompra;
    }

    /**
     * Define el valor de la propiedad valorCompra.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorCompra(BigDecimal value) {
        this.valorCompra = value;
    }

    /**
     * Obtiene el valor de la propiedad claveOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOperacion() {
        return claveOperacion;
    }

    /**
     * Define el valor de la propiedad claveOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOperacion(String value) {
        this.claveOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad correlativoRegistro.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCorrelativoRegistro() {
        return correlativoRegistro;
    }

    /**
     * Define el valor de la propiedad correlativoRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCorrelativoRegistro(BigDecimal value) {
        this.correlativoRegistro = value;
    }

}
