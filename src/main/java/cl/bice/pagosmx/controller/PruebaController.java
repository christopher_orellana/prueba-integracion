package cl.bice.pagosmx.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.bice.pagosmx.services.PruebaService;

@Controller
@ComponentScan("cl.bice.services")
@RequestMapping(value = "/prueba")
public class PruebaController {
	
	private final PruebaService pruebaService;
	
	@Autowired
	public PruebaController(final PruebaService pruebaService) {
		this.pruebaService = pruebaService;
	}

	@RequestMapping(value = "/consulta",
	        method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> consultaPago(@RequestParam String codigo, @RequestParam int numeroOperacion){
		try {
			Map<String, Object> respuesta = pruebaService.consultaPago(codigo, numeroOperacion);
			if(respuesta.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
