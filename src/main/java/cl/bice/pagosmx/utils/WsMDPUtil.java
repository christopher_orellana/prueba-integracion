package cl.bice.pagosmx.utils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.IntegracionMDP;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.IntegracionMesaBinding;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.MesaDeDineroPagoOnLinePeticionConsultarType;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionConsultar;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLineRespuesta;

public class WsMDPUtil {
	
	private static final QName SERVICE_NAME = new QName("http://ssf.ws/productos/servicios.pagos/integracionmesa/", "IntegracionMDP");
	private static URL wsdlURL = IntegracionMDP.WSDL_LOCATION;
	
	private static final IntegracionMDP ss = new IntegracionMDP(wsdlURL, SERVICE_NAME);
	private static final IntegracionMesaBinding port = ss.getIntegracionMesaBinding1();
	
	public PagoOnLineRespuesta consultarPago(PagoOnLinePeticionConsultar solicitud) {
		return port.consultarPago(solicitud);
	}

	public PagoOnLinePeticionConsultar generarSolicitud(String codigo, int numero){
		PagoOnLinePeticionConsultar pagoOnlinePeticionConsultar = new PagoOnLinePeticionConsultar();
		MesaDeDineroPagoOnLinePeticionConsultarType mesaDeDineroPagoOnLinePeticionConsultarType = new MesaDeDineroPagoOnLinePeticionConsultarType();
		mesaDeDineroPagoOnLinePeticionConsultarType.setCodigoProducto(codigo);
		mesaDeDineroPagoOnLinePeticionConsultarType.setNumeroOperacion(numero);
		pagoOnlinePeticionConsultar.setConsultas(mesaDeDineroPagoOnLinePeticionConsultarType);

		return pagoOnlinePeticionConsultar;
	}
	
}
