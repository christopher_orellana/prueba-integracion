package cl.bice.pagosmx.services;

import java.util.Map;

public interface PruebaService {

	Map<String, Object> consultaPago(String codigo, int numeroOperacion);
}
