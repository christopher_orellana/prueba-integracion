package cl.bice.pagosmx.services.impl;

import java.util.HashMap;
import java.util.Map;


import org.springframework.stereotype.Service;

import cl.bice.pagosmx.services.PruebaService;
import cl.bice.pagosmx.utils.WsMDPUtil;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.MesaDeDineroPagoOnLineRespuestaType;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLinePeticionConsultar;
import cl.bice.pagosmx.ws.productos.servicios.pagos.integracionmesa.types.PagoOnLineRespuesta;

@Service
public class PruebaServiceImpl implements PruebaService {

	@Override
	public Map<String, Object> consultaPago(String codigo, int numeroOperacion) {
		WsMDPUtil wsMDPUtil = new WsMDPUtil();
		
		PagoOnLinePeticionConsultar pagoOnlinePeticionConsultar = wsMDPUtil.generarSolicitud(codigo, numeroOperacion);
		PagoOnLineRespuesta pagoOnLineRespuesta = wsMDPUtil.consultarPago(pagoOnlinePeticionConsultar);
		if(pagoOnLineRespuesta != null && pagoOnLineRespuesta.getRespuesta() != null) {
			return generarRespuesta(pagoOnLineRespuesta);
		}
		
		return null;
	}

	private Map<String,Object> generarRespuesta(PagoOnLineRespuesta pagoOnLineRespuesta){
		Map<String,Object> respuesta = new HashMap<>();
		MesaDeDineroPagoOnLineRespuestaType mesaDeDineroPagoOnLineRespuestaType = pagoOnLineRespuesta.getRespuesta();
		respuesta.put("numeroOperacion", mesaDeDineroPagoOnLineRespuestaType.getNumeroOperacion());
		respuesta.put("detalleMensajesOperacion", mesaDeDineroPagoOnLineRespuestaType.getDetalleMensajesOperacion());
		respuesta.put("indicadorResultadoOperacion", mesaDeDineroPagoOnLineRespuestaType.getIndicadorResultadoOperacion());
		respuesta.put("mensajeResultadoOperacion", mesaDeDineroPagoOnLineRespuestaType.getMensajeResultadoOperacion());
		return respuesta;
	}

}
