package cl.bice.pagosmx.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;


@ApiModel(value = "EntityModel")

@Builder
@Data
public class EntityModel {
	
	/** 
	 * TODO
	 * Se deben agregar los atributos según sea la especificación de la tabla.
	 * 
	 * Se dejan atributos de ejemplo que deben ser reemplazados según corresponde 
	 * 
	 */

	
	@JsonProperty("ID")
	@ApiModelProperty(name = "id")
	private Long id;

	
	@JsonProperty("DESCRIPCION")
	@ApiModelProperty(name = "descripcion")
	private String descripcion;
	

}
