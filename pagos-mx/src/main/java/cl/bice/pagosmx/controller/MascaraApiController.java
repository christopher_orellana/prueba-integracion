package cl.bice.pagosmx.controller;

import cl.bice.logger.BiceLogger;
import cl.bice.pagosmx.services.PagosMxServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class MascaraApiController implements MascaraApi{

    private final BiceLogger biceLogger;
    private final PagosMxServices pagosMxServices;
 
    @Autowired
    public MascaraApiController(final PagosMxServices pagosMxServices, final BiceLogger biceLogger) {
        this.pagosMxServices = pagosMxServices;
        this.biceLogger = biceLogger;
    }

}