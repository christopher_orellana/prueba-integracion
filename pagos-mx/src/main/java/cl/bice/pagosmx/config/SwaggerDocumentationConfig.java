package cl.bice.pagosmx.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerDocumentationConfig {

    @Autowired
    private Environment environment;

    private static final String environmentProd = "prod";
    
    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("pagos-mx")
            .description("Pagos moneda extranjera")
            .license("")
            .licenseUrl("")
            .termsOfServiceUrl("")
            .version("1.0.0")
            .contact(new Contact("corellananex","", "corellananex@gmail.com"))
            .build();
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("cl.bice.pagosmx.controller"))
                    .build()
                .directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(java.time.OffsetDateTime.class, java.util.Date.class)
                .enable(verificaPerfilProduccion())
                .apiInfo(apiInfo());
    }

    private boolean verificaPerfilProduccion() {
        for (String perfil : environment.getActiveProfiles()) {
            if (perfil.contains(environmentProd)) {
                System.out.println("Se esta trabajando con el perfil [".concat(perfil).concat("] se deshabilita Swagger-UI."));
                return false;
            }
        }
        return true;
    }
}
