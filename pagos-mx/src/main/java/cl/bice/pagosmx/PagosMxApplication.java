package cl.bice.pagosmx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = "cl.bice.pagosmx")
@Import({cl.bice.srvbase.exception.ExceptionManager.class})
public class PagosMxApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagosMxApplication.class, args);
	}
	

}
