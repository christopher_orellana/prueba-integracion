package cl.bice.pagosmx.services;

import cl.bice.pagosmx.model.EntityModel;
import java.util.List;

public interface PagosMxServices {

	List<EntityModel> obtenerTodos();

}
