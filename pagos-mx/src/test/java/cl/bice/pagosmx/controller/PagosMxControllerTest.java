package cl.bice.pagosmx.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import cl.bice.pagosmx.services.PagosMxServices;


@DisplayName("Descripcion del test")
@RunWith(MockitoJUnitRunner.class)
public class PagosMxControllerTest {
	
	
	
	@Mock
	private PagosMxServices services;
	
	
	
	@Before
	public void init() {

	}
	
	@Test
	@DisplayName("Descripcion del escenario que se prueba")
	public void obtenerTodosOk() {
		
		assertEquals(HttpStatus.OK, HttpStatus.OK);;
	}
	

}
